﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS
{
    public partial class Loading : Form
    {
        public Action Worker { get; set; }
        private ImageList imgList { get; set; }
        private Timer timer = new Timer();
        private int i = 0;


        public Loading(Action worker)
        {
            InitializeComponent();
            if (worker == null)
                throw new ArgumentNullException();
            Worker = worker;
        }

        public Loading()
        {
            InitializeComponent();
            loadStart();
        }

        //protected override void OnLoad(EventArgs e)
        //{
        //    base.OnLoad(e);
        //    Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        //}

        private void Loading_Load(object sender, EventArgs e)
        {
            
        }

        void loadStart()
        {
            populateImage();
            timer = new Timer();
            timer.Interval = 100;
            timer.Start();
            timer.Tick += new EventHandler(timerLapsed);
        }

        void populateImage()
        {
            Bitmap[] imgPath = {
                Properties.Resources._1,
                Properties.Resources._2,
                Properties.Resources._3,
                Properties.Resources._4,
                Properties.Resources._5,
                Properties.Resources._6,
                Properties.Resources._7,
                Properties.Resources._8,
            };
            ImageList lst = new ImageList();
            lst.ImageSize = new Size(50, 50);
            lst.Images.AddRange(imgPath);
            imgList = lst;
        }

        private void timerLapsed(object sender, EventArgs args)
        {
            if (i == imgList.Images.Count - 1)
            {
                i = 0;
            }
            pictureBox1.Image = imgList.Images[i];
            i++;
        }

        private delegate DialogResult InvokeDelegate(Form parent);
        public DialogResult LoadShowDialog(Form parent)
        {
            InvokeDelegate d = new InvokeDelegate(LoadShowDialog);
            object[] o = new object[] { parent };
            if (parent.InvokeRequired)
            {
                return (DialogResult)parent.Invoke(d, o);
            }
            return this.ShowDialog(parent);
        }
    }
}
