﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Lib
{
    public static class SupplierControl
    {
        private static string ErrMessage { get; set; }
        private static bool res = false;
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        private static DataTable dt()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Supplier", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("Phone", typeof(string));
            dt.Columns.Add("Alamat", typeof(string));

            return dt;
        }
        public static object loadSupplier()
        {
            DataTable dt = Lib.SupplierControl.dt();
            var supplierList = db.dbo_supplier
                .OrderByDescending(a => a.id_supplier).ToList();
            foreach (var i in supplierList)
            {
                dt.Rows.Add(i.id_supplier, i.nama_supplier, i.email, i.phone, i.alamat);
            }

            return dt;
        }

        public static object searchSupplier(string search)
        {
            DataTable dt = Lib.SupplierControl.dt();
            if (search != "")
            {
                var supplierList = db.dbo_supplier
                .Where(a => a.nama_supplier.Contains(search) || a.phone.Contains(search) || a.email.Contains(search) || a.alamat.Contains(search))
                .OrderByDescending(a => a.id_supplier).ToList();
                foreach (var i in supplierList)
                {
                    dt.Rows.Add(i.id_supplier, i.nama_supplier, i.email, i.phone, i.alamat);
                }
            }
            else
            {
                var supplierList = db.dbo_supplier
                .OrderByDescending(a => a.id_supplier).ToList();
                foreach (var i in supplierList)
                {
                    dt.Rows.Add(i.id_supplier, i.nama_supplier, i.email, i.phone, i.alamat);
                }
            }

            return dt;
        }

        public static bool CRUD(int method, AppData.dbo_supplier supplier)
        {
            db = new AppData.InventoryEntities();
            PasswordHash PassHash = new PasswordHash();
            switch (method)
            {
                case 0:
                    db.dbo_supplier.Add(new AppData.dbo_supplier
                    {
                        nama_supplier = supplier.nama_supplier,
                        email = supplier.email,
                        phone = supplier.phone,
                        alamat = supplier.alamat
                    });
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Add Supplier Error : " + ex.Message;
                    }
                    break;
                case 1:
                    var Supplier_ = db.dbo_supplier.Where(a => a.id_supplier == supplier.id_supplier).FirstOrDefault();
                    Supplier_.nama_supplier = supplier.nama_supplier;
                    Supplier_.phone = supplier.phone;
                    Supplier_.email = supplier.email;
                    Supplier_.alamat = supplier.alamat;
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Update Supplier error : " + ex.Message;
                    }

                    break;
                case 2:
                    var removeSupplier = db.dbo_supplier.Where(a => a.id_supplier == supplier.id_supplier).FirstOrDefault();
                    try
                    {
                        db.dbo_supplier.Remove(removeSupplier);
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Remove user error " + ex.Message;
                    }
                    break;
                default:
                    res = false;
                    ErrMessage += " No Data ";
                    break;
            }
            return res;
        }

        public static object loadSupplierList()
        {
            db = new AppData.InventoryEntities();
            var supplierList = db.dbo_supplier
                .OrderByDescending(a => a.id_supplier)
                .Select(a => new { a.id_supplier, a.nama_supplier})
                .ToList();

            return supplierList;
        }
    }
}
