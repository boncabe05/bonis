﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Lib
{
    public static class BarangKeluarControl
    {
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        public static string ErrMessage { get; set; }

        private static DataTable DT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("IDCustomer", typeof(int));
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Tanggal", typeof(string));
            return dt;
        }

        public static object loadBarangKeluar()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.BarangKeluarControl.DT();
            var barangList = db.dbo_barang_keluar
                .Join(db.dbo_customer, a => a.id_customer, b => b.id_customer, (a, b) =>
                new { a.id_keluar, b.id_customer, b.nama_customer, a.tanggal })
                .OrderByDescending(a => a.id_keluar).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_keluar, i.id_customer, i.nama_customer, i.tanggal);
            }
            return dt;
        }

        public static object searchBarangKeluar(string search)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.BarangKeluarControl.DT();
            var barangList = db.dbo_barang_keluar
                .Join(db.dbo_customer, a => a.id_customer, b => b.id_customer, (a, b) =>
                new { a.id_keluar, b.id_customer, b.nama_customer, a.tanggal })
                .Where(a => a.nama_customer.Contains(search) || a.tanggal.Contains(search))
                .OrderByDescending(a => a.id_keluar).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_keluar, i.id_customer, i.nama_customer, i.tanggal);
            }
            return dt;
        }

        public static List<BarangKeluarLItem> loadBarangKeluarList(int idKeluar)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            var barangList = db.dbo_barang_keluar_LITEM
                .Where(a => a.id_barang_keluar == idKeluar)
                .Select(a => new BarangKeluarLItem
                {
                    idBarangKeluarLitem = a.id_LITEM_keluar,
                    idBarang = a.id_barang,
                    Jumlah = a.jumlah
                })
                .ToList();

            return barangList;
        }

        public static void loadSatuanList(DataGridViewComboBoxColumn cbo)
        {
            db = new AppData.InventoryEntities();
            var satuanList = db.dbo_satuan.OrderByDescending(a => a.id_satuan)
                .Select(a => new { a.id_satuan, a.nama_satuan }).ToList();
            cbo.DataSource = satuanList;
            cbo.ValueMember = "id_satuan";
            cbo.DisplayMember = "nama_satuan";
        }

        public static object loadBarang()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            var barangList = db.dbo_barang
                .OrderByDescending(a => a.id_barang)
                .Select(a => new {  a.id_barang, a.nama_barang })
                .ToList();

            return barangList;
        }

        public static object loadJumlah()
        {
            var jmlList = new List<TempJumlah>();
            for (int i = 1; i < 100; i++)
            {
                jmlList.Add(new TempJumlah { idjumlah = i, jumlah = i });
            }
            return jmlList;
        }

        public static bool checkStokBarang(int id, int stok)
        {
            db = new AppData.InventoryEntities();
            var maxStok = db.dbo_barang
                .Where(a => a.id_barang == id)
                .Select(a => a.stok).FirstOrDefault();
            bool res = (stok > maxStok) ? true : false;
            return res;
        }

        public static bool Crud(int method, 
            AppData.dbo_barang_keluar barang_keluar = null, 
            List<BarangKeluarLItem> param_ = null, 
            string idBarangKeluar = null)
        {
            bool res = false;
            db = new AppData.InventoryEntities();
            switch (method)
            {
                case 0:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        db.dbo_barang_keluar.Add(barang_keluar);
                        db.SaveChanges();
                        int id_barang_keluar = barang_keluar.id_keluar;
                        try
                        {
                            AppData.InventoryEntities dbBarangKeluar = new AppData.InventoryEntities();
                            AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                            foreach (var param in param_)
                            {
                                var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                stokBarang.stok = stokBarang.stok - param.Jumlah;
                               
                                dbBarangKeluar.dbo_barang_keluar_LITEM.Add(new AppData.dbo_barang_keluar_LITEM
                                {
                                    id_barang_keluar = id_barang_keluar,
                                    id_barang = param.idBarang,
                                    jumlah = param.Jumlah
                                });
                                try
                                {
                                    if (dbBarang.SaveChanges() > 0 && dbBarangKeluar.SaveChanges() > 0)
                                    {
                                        res = true;
                                    }
                                }
                                catch (Exception ee)
                                {
                                    ErrMessage += ee.Message;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage += ex.Message;
                        }
                    }catch(Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                case 1:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        AppData.dbo_barang_keluar_LITEM BarangKeluarLItem = new AppData.dbo_barang_keluar_LITEM();
                        int idKeluar = Convert.ToInt32(idBarangKeluar);
                        var barangKeluar = db.dbo_barang_keluar.Where(a => a.id_keluar == idKeluar).FirstOrDefault();
                        barangKeluar.id_customer = barang_keluar.id_customer;
                        barangKeluar.tanggal = barang_keluar.tanggal;
                        barangKeluar.user_id = barang_keluar.user_id;
                        db.SaveChanges();
                        int id_barang_keluar = Convert.ToInt32(idBarangKeluar);
                        try
                        {
                            AppData.InventoryEntities dbBarangKeluar = new AppData.InventoryEntities();
                            AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                            foreach (var param in param_)
                            {
                                var exist = db.dbo_barang_keluar_LITEM.Where(a => a.id_LITEM_keluar == param.idBarangKeluarLitem).Any();
                                if (exist)
                                {
                                    if (!param.isDeleted)
                                    {
                                        var stokSebelumnya = dbBarang.dbo_barang_keluar_LITEM.Where(a => a.id_barang == param.idBarang && a.id_LITEM_keluar == param.idBarangKeluarLitem).Select(a => a.jumlah).FirstOrDefault();
                                        var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                        if (param.Jumlah != stokSebelumnya)
                                        {
                                            var hasil = (stokBarang.stok + stokSebelumnya) - param.Jumlah;
                                            stokBarang.stok = hasil;
                                            try
                                            {
                                                if (dbBarang.SaveChanges() > 0)
                                                {
                                                    res = true;
                                                }
                                            }
                                            catch(Exception ee)
                                            {
                                                ErrMessage += ee.Message;
                                            }
                                        }
                                        BarangKeluarLItem = dbBarangKeluar.dbo_barang_keluar_LITEM.Where(x => x.id_LITEM_keluar == param.idBarangKeluarLitem && x.id_barang_keluar == id_barang_keluar).FirstOrDefault();
                                        BarangKeluarLItem.id_barang = param.idBarang;
                                        BarangKeluarLItem.jumlah = param.Jumlah;
                                        try
                                        {
                                            dbBarangKeluar.SaveChanges();
                                        }
                                        catch (Exception ee)
                                        {
                                            ErrMessage += ee.Message;
                                        }
                                       
                                    }
                                    else
                                    {
                                        
                                        var stokSebelumnya = dbBarang.dbo_barang_keluar_LITEM.Where(a => a.id_barang == param.idBarang && a.id_LITEM_keluar == param.idBarangKeluarLitem).Select(a => a.jumlah).FirstOrDefault();
                                        var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                        var hasil = stokBarang.stok + stokSebelumnya;
                                        stokBarang.stok = hasil;
                                        BarangKeluarLItem = dbBarangKeluar.dbo_barang_keluar_LITEM.Where(x => x.id_LITEM_keluar == param.idBarangKeluarLitem).FirstOrDefault();
                                        dbBarangKeluar.dbo_barang_keluar_LITEM.Remove(BarangKeluarLItem);
                                        if (dbBarang.SaveChanges() > 0 && dbBarangKeluar.SaveChanges() > 0)
                                        {
                                            res = true;
                                        }
                                    }
                                }
                                else
                                {
                                    var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                    stokBarang.stok = stokBarang.stok - param.Jumlah;
                                    dbBarangKeluar.dbo_barang_keluar_LITEM.Add(new AppData.dbo_barang_keluar_LITEM
                                    {
                                        id_barang_keluar = id_barang_keluar,
                                        id_barang = param.idBarang,
                                        jumlah = param.Jumlah
                                    });
                                    if (dbBarang.SaveChanges() > 0 && dbBarangKeluar.SaveChanges() > 0)
                                    {
                                        res = true;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage += ex.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                case 2:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        AppData.InventoryEntities dbBarangKeluar = new AppData.InventoryEntities();
                        AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                        int idBarang_Keluar = Convert.ToInt32(idBarangKeluar);
                        var del = db.dbo_barang_keluar
                            .Where(a => a.id_keluar == idBarang_Keluar)
                            .FirstOrDefault();
                        var barangKeluarList = dbBarangKeluar.dbo_barang_keluar_LITEM
                            .Where(a => a.id_barang_keluar == del.id_keluar)
                            .ToList();
                        foreach(var barangList in barangKeluarList)
                        {
                            var barang = dbBarang.dbo_barang.Where(a => a.id_barang == barangList.id_barang).FirstOrDefault();
                            barang.stok = barang.stok + barangList.jumlah;
                        }

                        dbBarangKeluar.dbo_barang_keluar_LITEM
                            .RemoveRange(dbBarangKeluar.dbo_barang_keluar_LITEM
                            .Where(a => a.id_barang_keluar == idBarang_Keluar));
                        db.dbo_barang_keluar.Remove(del);
                        try
                        {
                            if(db.SaveChanges() > 0 
                                && dbBarang.SaveChanges() > 0 
                                && dbBarangKeluar.SaveChanges() > 0)
                            {
                                res = true;
                            }
                        }catch(Exception ee)
                        {
                            ErrMessage += ee.Message;

                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                default:
                    ErrMessage += "Tidak memilih apapun";
                    break;
            }
            return res;
        }
    }

    public class TempJumlah
    {
        public int idjumlah { get; set; }
        public int jumlah {get; set; }
    }

    public class BarangKeluarLItem
    {
        public int idBarangKeluar { get; set; }
        public int idBarangKeluarLitem { get; set; }
        public int idBarang { get; set; }
        public int Jumlah { get; set; }
        public bool isDeleted { get; set; }
    }
}
