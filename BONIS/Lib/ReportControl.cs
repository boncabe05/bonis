﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BONIS.Lib
{
    public class ReportControl
    {
        public static class Barang
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dataTable_()
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("nama_barang", typeof(string));
                dt.Columns.Add("stok", typeof(int));
                dt.Columns.Add("nama_satuan", typeof(string));
                return dt;
            }

            public static DataTable loadBarang()
            {
                DataTable dt = Lib.ReportControl.Barang.dataTable_();
                var barangList = db.dbo_barang
                .Join(db.dbo_satuan, a => a.id_satuan, b => b.id_satuan, (a, b) =>
                new { a.nama_barang, a.stok, b.nama_satuan }).ToArray();
                foreach (var i in barangList)
                {
                    dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                }

                return dt;
            }

            public static DataTable searchBarang(string searchBy, string search)
            {
                search = search.ToLower();
                DataTable dt = Lib.ReportControl.Barang.dataTable_();
                var barangList = db.dbo_barang
                .Join(db.dbo_satuan, a => a.id_satuan, b => b.id_satuan, (a, b) =>
                new { a.nama_barang, a.stok, b.nama_satuan }).ToList();
                switch (searchBy)
                {
                    case "barang":
                        barangList = barangList.Where(a => a.nama_barang.ToLower().Contains(search)).ToList();
                        foreach (var i in barangList)
                        {
                            dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                        }
                        break;
                    case "stok":
                        barangList = barangList.Where(a => a.stok.ToString().ToLower().Contains(search)).ToList();
                        foreach (var i in barangList)
                        {
                            dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                        }
                        break;
                    case "satuan":
                        barangList = barangList.Where(a => a.nama_satuan.ToLower().Contains(search)).ToList();
                        foreach (var i in barangList)
                        {
                            dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                        }
                        break;
                    case "none":
                        foreach (var i in barangList)
                        {
                            dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                        }
                        break;
                    default:
                        foreach (var i in barangList)
                        {
                            dt.Rows.Add(i.nama_barang, i.stok, i.nama_satuan);
                        }
                        break;
                }

                return dt;
            }
        }

        public static class Customer
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dt = new DataTable();
            
            public static DataTable loadCustomer()
            {
                dt = new DataTable();
                dt.Columns.Add("id_customer", typeof(int));
                dt.Columns.Add("nama_customer", typeof(string));
                dt.Columns.Add("phone", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("alamat", typeof(string));
                var custList = db.dbo_customer.ToList();
                foreach (var i in custList)
                {
                    dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                }

                return dt;
            }

            public static DataTable searchCustomer(string searchBy, string search)
            {
                search = search.ToLower();
                dt = new DataTable();
                dt.Columns.Add("id_customer", typeof(int));
                dt.Columns.Add("nama_customer", typeof(string));
                dt.Columns.Add("phone", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("alamat", typeof(string));
                var custList = db.dbo_customer
                    .OrderByDescending(a => a.id_customer)
                    .ToList();
                switch (searchBy)
                {
                    case "nama":
                        custList = custList.Where(a => a.nama_customer.ToLower().Contains(search)).ToList();
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "phone":
                        custList = custList.Where(a => a.phone.ToLower().Contains(search)).ToList();
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "email":
                        custList = custList.Where(a => a.email.ToLower().Contains(search)).ToList();
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "alamat":
                        custList = custList.Where(a => a.alamat.ToLower().Contains(search)).ToList();
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "none":
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                    default:
                        foreach (var i in custList)
                        {
                            dt.Rows.Add(i.id_customer, i.nama_customer, i.phone, i.email, i.alamat);
                        }
                        break;
                }

                return dt;
            }
        }

        public static class Supplier
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dt = new DataTable();

            public static DataTable loadSupplier()
            {
                dt = new DataTable();
                dt.Columns.Add("id_supplier", typeof(string));
                dt.Columns.Add("nama_supplier", typeof(string));
                dt.Columns.Add("phone", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("alamat", typeof(string));
                var supList = db.dbo_supplier.OrderByDescending(a => a.id_supplier).ToList();
                foreach (var i in supList)
                {
                    dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                }

                return dt;
            }

            public static DataTable searchSupplier(string searchBy, string search)
            {
                search = search.ToLower();
                dt = new DataTable();
                dt.Columns.Add("id_supplier", typeof(string));
                dt.Columns.Add("nama_supplier", typeof(string));
                dt.Columns.Add("phone", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("alamat", typeof(string));
                var supList = db.dbo_supplier
                    .OrderByDescending(a => a.id_supplier)
                    .ToList();
                switch (searchBy)
                {
                    case "nama":
                        supList = supList.Where(a => a.nama_supplier.ToLower().Contains(search)).ToList();
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "phone":
                        supList = supList.Where(a => a.phone.ToLower().Contains(search)).ToList();
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "email":
                        supList = supList.Where(a => a.email.ToLower().Contains(search)).ToList();
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "alamat":
                        supList = supList.Where(a => a.alamat.ToLower().Contains(search)).ToList();
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                    case "none":
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                    default:
                        foreach (var i in supList)
                        {
                            dt.Rows.Add(i.id_supplier, i.nama_supplier, i.phone, i.email, i.alamat);
                        }
                        break;
                }

                return dt;
            }
        }

        public static class BarangKeluar
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dt()
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("id_keluar", typeof(string));
                dt.Columns.Add("nama_customer", typeof(string));
                dt.Columns.Add("tanggal", typeof(string));
                dt.Columns.Add("staff_name", typeof(string));
                dt.Columns.Add("nama_barang", typeof(string));
                dt.Columns.Add("jumlah", typeof(string));
                dt.Columns.Add("nama_satuan", typeof(string));
                return dt;
            }

            public static DataTable loadBarangKeluar()
            {
                DataTable dt = Lib.ReportControl.BarangKeluar.dt();
                var barangKeluarList = (from bk in db.dbo_barang_keluar
                                       join bkitem in db.dbo_barang_keluar_LITEM on bk.id_keluar equals bkitem.id_barang_keluar
                                       join br in db.dbo_barang on bkitem.id_barang equals br.id_barang
                                       join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                       join cs in db.dbo_customer on bk.id_customer equals cs.id_customer
                                       join sf in db.dbo_staff on bk.user_id equals sf.user_id
                                       select new
                                       {
                                           bk.id_keluar,
                                           cs.nama_customer,
                                           bk.tanggal,
                                           sf.staff_name,
                                           br.nama_barang,
                                           bkitem.jumlah,
                                           st.nama_satuan
                                       }).ToList();
                foreach (var i in barangKeluarList)
                {
                    dt.Rows.Add(i.id_keluar,
                                i.nama_customer,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }

            public static DataTable searchBarangKeluar(string searchBy, string search, string periode, DateTime date)
            {
                search = search.ToLower();
                DataTable dt = Lib.ReportControl.BarangKeluar.dt();
                var barangKeluarList = (from bk in db.dbo_barang_keluar
                                        join bkitem in db.dbo_barang_keluar_LITEM on bk.id_keluar equals bkitem.id_barang_keluar
                                        join br in db.dbo_barang on bkitem.id_barang equals br.id_barang
                                        join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                        join cs in db.dbo_customer on bk.id_customer equals cs.id_customer
                                        join sf in db.dbo_staff on bk.user_id equals sf.user_id
                                        select new
                                        {
                                            bk.id_keluar,
                                            cs.nama_customer,
                                            bk.tanggal,
                                            sf.staff_name,
                                            br.nama_barang,
                                            bkitem.jumlah,
                                            st.nama_satuan
                                        }).ToList();
                switch (periode)
                {
                    case "harian":
                        string harian_ = date.ToString("dd/MM/yyyy");
                        barangKeluarList = barangKeluarList.Where(a => DateTime.Parse(a.tanggal).ToString("dd/MM/yyyy").Contains(harian_)).ToList();
                        break;
                    case "mingguan":
                        
                        break;
                    case "bulanan":
                        string bulanan_ = date.ToString("MM/yyyy");
                        barangKeluarList = barangKeluarList.Where(a => DateTime.Parse(a.tanggal).ToString("MM/yyyy").Contains(bulanan_)).ToList();
                        break;
                    case "tahunan":
                        string tahunan_ = date.ToString("yyyy");
                        barangKeluarList = barangKeluarList.Where(a => DateTime.Parse(a.tanggal).ToString("yyyy").Contains(tahunan_)).ToList();
                        break;
                    default:
                        break;
                }

                switch (searchBy)
                {
                    case "customer":
                        barangKeluarList = barangKeluarList.Where(a => a.nama_customer.ToLower().Contains(search)).ToList();
                        break;
                    case "staff":
                        barangKeluarList = barangKeluarList.Where(a => a.staff_name.ToLower().Contains(search)).ToList();
                        break;
                    case "barang":
                        barangKeluarList = barangKeluarList.Where(a => a.nama_barang.ToLower().Contains(search)).ToList();
                        break;
                    case "satuan":
                        barangKeluarList = barangKeluarList.Where(a => a.nama_satuan.ToLower().Contains(search)).ToList();
                        break;
                    case "jumlah":
                        barangKeluarList = barangKeluarList.Where(a => a.jumlah.ToString().ToLower().Contains(search)).ToList();
                        break;
                    case "tanggal":
                        barangKeluarList = barangKeluarList.Where(a => a.nama_customer.ToLower().Contains(search)).ToList();
                        break;
                    default:
                        
                        break;
                }
                foreach (var i in barangKeluarList)
                {
                    dt.Rows.Add(i.id_keluar,
                                i.nama_customer,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }

            public static DataTable searchDateBetween(DateTime dateMin, DateTime dateMax)
            {
                DataTable dt = Lib.ReportControl.BarangKeluar.dt();
                var barangKeluarList = (from bk in db.dbo_barang_keluar
                                        join bkitem in db.dbo_barang_keluar_LITEM on bk.id_keluar equals bkitem.id_barang_keluar
                                        join br in db.dbo_barang on bkitem.id_barang equals br.id_barang
                                        join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                        join cs in db.dbo_customer on bk.id_customer equals cs.id_customer
                                        join sf in db.dbo_staff on bk.user_id equals sf.user_id
                                        select new
                                        {
                                            bk.id_keluar,
                                            cs.nama_customer,
                                            bk.tanggal,
                                            sf.staff_name,
                                            br.nama_barang,
                                            bkitem.jumlah,
                                            st.nama_satuan
                                        }).ToList();

                barangKeluarList = barangKeluarList.Where(a => Convert.ToDateTime(a.tanggal) >= dateMin && Convert.ToDateTime(a.tanggal) <= dateMax).ToList();


                foreach (var i in barangKeluarList)
                {
                    dt.Rows.Add(i.id_keluar,
                                i.nama_customer,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }
        }

        public static class BarangMasuk
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dt()
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("id_masuk", typeof(string));
                dt.Columns.Add("nama_supplier", typeof(string));
                dt.Columns.Add("tanggal", typeof(string));
                dt.Columns.Add("staff_name", typeof(string));
                dt.Columns.Add("nama_barang", typeof(string));
                dt.Columns.Add("jumlah", typeof(string));
                dt.Columns.Add("nama_satuan", typeof(string));
                return dt;
            }

            public static DataTable loadBarangMasuk()
            {
                DataTable dt = Lib.ReportControl.BarangMasuk.dt();
                var barangMasukList = (from bm in db.dbo_barang_masuk
                                        join bmitem in db.dbo_barang_masuk_LITEM on bm.id_masuk equals bmitem.id_barang_masuk
                                        join br in db.dbo_barang on bmitem.id_barang equals br.id_barang
                                        join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                        join sp in db.dbo_supplier on bm.id_supplier equals sp.id_supplier
                                        join sf in db.dbo_staff on bm.user_id equals sf.user_id
                                        select new
                                        {
                                            bm.id_masuk,
                                            sp.nama_supplier,
                                            bm.tanggal,
                                            sf.staff_name,
                                            br.nama_barang,
                                            bmitem.jumlah,
                                            st.nama_satuan
                                        }).ToList();
                foreach (var i in barangMasukList)
                {
                    dt.Rows.Add(i.id_masuk,
                                i.nama_supplier,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }

            public static DataTable searchBarangMasuk(string searchBy, string search, string periode, DateTime date)
            {
                search = search.ToLower();
                DataTable dt = Lib.ReportControl.BarangMasuk.dt();
                var barangMasukList = (from bm in db.dbo_barang_masuk
                                       join bmitem in db.dbo_barang_masuk_LITEM on bm.id_masuk equals bmitem.id_barang_masuk
                                       join br in db.dbo_barang on bmitem.id_barang equals br.id_barang
                                       join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                       join sp in db.dbo_supplier on bm.id_supplier equals sp.id_supplier
                                       join sf in db.dbo_staff on bm.user_id equals sf.user_id
                                       select new
                                       {
                                           bm.id_masuk,
                                           sp.nama_supplier,
                                           bm.tanggal,
                                           sf.staff_name,
                                           br.nama_barang,
                                           bmitem.jumlah,
                                           st.nama_satuan
                                       }).ToList();
                switch (periode)
                {
                    case "harian":
                        string harian_ = date.ToString("dd/MM/yyyy");
                        barangMasukList = barangMasukList.Where(a => DateTime.Parse(a.tanggal).ToString("dd/MM/yyyy").Contains(harian_)).ToList();
                        break;
                    case "mingguan":

                        break;
                    case "bulanan":
                        string bulanan_ = date.ToString("MM/yyyy");
                        barangMasukList = barangMasukList.Where(a => DateTime.Parse(a.tanggal).ToString("MM/yyyy").Contains(bulanan_)).ToList();
                        break;
                    case "tahunan":
                        string tahunan_ = date.ToString("yyyy");
                        barangMasukList = barangMasukList.Where(a => DateTime.Parse(a.tanggal).ToString("yyyy").Contains(tahunan_)).ToList();
                        break;
                    default:
                        break;
                }

                switch (searchBy)
                {
                    case "supplier":
                        barangMasukList = barangMasukList.Where(a => a.nama_supplier.ToLower().Contains(search)).ToList();
                        break;
                    case "staff":
                        barangMasukList = barangMasukList.Where(a => a.staff_name.ToLower().Contains(search)).ToList();
                        break;
                    case "barang":
                        barangMasukList = barangMasukList.Where(a => a.nama_barang.ToLower().Contains(search)).ToList();
                        break;
                    case "satuan":
                        barangMasukList = barangMasukList.Where(a => a.nama_satuan.ToLower().Contains(search)).ToList();
                        break;
                    case "jumlah":
                        barangMasukList = barangMasukList.Where(a => a.jumlah.ToString().ToLower().Contains(search)).ToList();
                        break;
                    case "tanggal":
                        //barangMasukList = barangMasukList.Where(a => a.nama_customer.ToLower().Contains(search)).ToList();
                        break;
                    default:

                        break;
                }
                foreach (var i in barangMasukList)
                {
                    dt.Rows.Add(i.id_masuk,
                                i.nama_supplier,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }

            public static DataTable searchDateBetween(DateTime dateMin, DateTime dateMax)
            {
                DataTable dt = Lib.ReportControl.BarangMasuk.dt();
                var barangMasukList = (from bm in db.dbo_barang_masuk
                                       join bmitem in db.dbo_barang_masuk_LITEM on bm.id_masuk equals bmitem.id_barang_masuk
                                       join br in db.dbo_barang on bmitem.id_barang equals br.id_barang
                                       join st in db.dbo_satuan on br.id_satuan equals st.id_satuan
                                       join sp in db.dbo_supplier on bm.id_supplier equals sp.id_supplier
                                       join sf in db.dbo_staff on bm.user_id equals sf.user_id
                                       select new
                                       {
                                           bm.id_masuk,
                                           sp.nama_supplier,
                                           bm.tanggal,
                                           sf.staff_name,
                                           br.nama_barang,
                                           bmitem.jumlah,
                                           st.nama_satuan
                                       }).ToList();

                barangMasukList = barangMasukList.Where(a => Convert.ToDateTime(a.tanggal) >= dateMin && Convert.ToDateTime(a.tanggal) <= dateMax).ToList();


                foreach (var i in barangMasukList)
                {
                    dt.Rows.Add(i.id_masuk,
                                i.nama_supplier,
                                DateTime.Parse(i.tanggal).ToString("dd/MM/yyyy"),
                                i.staff_name,
                                i.nama_barang,
                                i.jumlah,
                                i.nama_satuan);
                }

                return dt;
            }
        }



        public static class Staff
        {
            private static AppData.InventoryEntities db = new AppData.InventoryEntities();
            private static DataTable dt(bool includeUsername)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("staff_name", typeof(string));
                dt.Columns.Add("email", typeof(string));
                dt.Columns.Add("phone", typeof(string));
                if (includeUsername)
                {
                    dt.Columns.Add("username", typeof(string));
                }
                return dt;
            }

            public static DataTable loadStaff(bool includeUsername)
            {
                DataTable dt = Lib.ReportControl.Staff.dt(includeUsername);
                var staffList = db.dbo_staff
                    .Join(db.dbo_user, a => a.user_id, b => b.user_id, (a, b) =>
                    new { a.staff_id, a.staff_name, a.email, a.phone, b.username })
                    .OrderByDescending(a => a.staff_id).ToList();
                if (includeUsername)
                {
                    foreach (var i in staffList)
                    {
                        dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                    }
                }
                else
                {
                    foreach (var i in staffList)
                    {
                        dt.Rows.Add(i.staff_name, i.email, i.phone);
                    }
                }

                return dt;
            }

            public static DataTable searchStaff(string searchBy, string search, bool includeUsername)
            {
                search = search.ToLower();
                DataTable dt = Lib.ReportControl.Staff.dt(includeUsername);
                var staffList = db.dbo_staff
                    .Join(db.dbo_user, a => a.user_id, b => b.user_id, (a, b) =>
                    new { a.staff_id, a.staff_name, a.email, a.phone, b.username })
                    .OrderByDescending(a => a.staff_id).ToList();
                switch (searchBy)
                {
                    case "nama":
                        staffList = staffList.Where(a => a.staff_name.ToLower().Contains(search)).ToList();
                        if (includeUsername)
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                    case "phone":
                        staffList = staffList.Where(a => a.phone.ToLower().Contains(search)).ToList();
                        if (includeUsername)
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                    case "email":
                        staffList = staffList.Where(a => a.email.ToLower().Contains(search)).ToList();
                        if (includeUsername)
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                    case "username":

                        if (includeUsername)
                        {
                            staffList = staffList.Where(a => a.username.ToLower().Contains(search)).ToList();
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                    case "none":
                        if (includeUsername)
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                    default:
                        if (includeUsername)
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone, i.username);
                            }
                        }
                        else
                        {
                            foreach (var i in staffList)
                            {
                                dt.Rows.Add(i.staff_name, i.email, i.phone);
                            }
                        }
                        break;
                }

                return dt;
            }
        }
    }
}
