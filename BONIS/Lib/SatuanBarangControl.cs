﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Lib
{
    public static class SatuanBarangControl
    {
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        public static string ErrMessage { get; set; }

        private static DataTable DT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Satuan", typeof(string));
            return dt;
        }

        public static object loadSatuan()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.SatuanBarangControl.DT();
            var satuanList = db.dbo_satuan.OrderByDescending(a => a.id_satuan)
                .Select(a => new { a.id_satuan, a.nama_satuan }).ToList();
            foreach (var i in satuanList)
            {
                dt.Rows.Add(i.id_satuan, i.nama_satuan);
            }
            return dt;
        }

        public static object searchSatuan(string search)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.SatuanBarangControl.DT();
            var satuanList = db.dbo_satuan.Where(a => a.nama_satuan.Contains(search)).OrderByDescending(a => a.id_satuan)
                .Select(a => new { a.id_satuan, a.nama_satuan }).ToArray();
            foreach (var i in satuanList)
            {
                dt.Rows.Add(i.id_satuan, i.nama_satuan);
            }
            return dt;
        }

        public static bool Crud(int method, AppData.dbo_satuan param)
        {
            bool res = false;
            db = new AppData.InventoryEntities();
            switch (method)
            {
                case 0:
                    db.dbo_satuan.Add(new AppData.dbo_satuan
                    {
                        nama_satuan = param.nama_satuan
                    });
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage = ex.Message;
                    }
                    break;
                case 1:
                    AppData.dbo_satuan satuan;
                    satuan = db.dbo_satuan.Where(a => a.id_satuan == param.id_satuan).FirstOrDefault<AppData.dbo_satuan>();
                    if (satuan != null)
                    {
                        satuan.nama_satuan = param.nama_satuan;
                        try
                        {
                            if (db.SaveChanges() > 0)
                            {
                                res = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage = ex.Message;
                        }
                    }
                    
                    break;
                case 2:
                    var del = db.dbo_satuan.Where(a => a.id_satuan == param.id_satuan).FirstOrDefault();
                    db.dbo_satuan.Remove(del);
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage = ex.Message;
                    }
                    break;
                default:
                    ErrMessage = "Tidak memilih apapun";
                    break;
            }
            return res;
        }

        public static void loadSatuanList(ComboBox cbo)
        {
            db = new AppData.InventoryEntities();
            var satuanList = db.dbo_satuan.OrderByDescending(a => a.id_satuan)
                .Select(a => new { a.id_satuan, a.nama_satuan }).ToList();
            cbo.DataSource = satuanList;
            cbo.ValueMember = "id_satuan";
            cbo.DisplayMember = "nama_satuan";
        }
    }
}
