﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BONIS.Lib
{
    public static class ManageUser
    {
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        public static string ErrMessage = "";
        public static string User = "";
        public static int UID = 0;
        public static int RID = 0;
        public static int SID = 0;
        public static string StaffName { get; set; }
        public static bool CheckUser = false;

        public static bool CheckLogin(string user, string pass)
        {
            bool check = false;
            bool uid = db.dbo_user.Any(a => a.username == user);
            if (uid)
            {

                var pwd = db.dbo_user.Where(a => a.username == user).Select(a =>
                a.password).FirstOrDefault();
                var success = PasswordHash.ValidatePassword(pass, pwd);
                CheckUser = true;
                if(success)
                {
                    UID = db.dbo_user.Where(a => a.username == user).Select(a => a.user_id).FirstOrDefault();
                    SID = db.dbo_staff.Where(a => a.user_id == UID).Select(a => a.staff_id).FirstOrDefault();
                    //RID = db.staffs.Where(a => a.staff_id == SID).Select(a => a.role_id).FirstOrDefault();
                    StaffName = db.dbo_staff.Where(a => a.user_id == UID).Select(a => a.staff_name).FirstOrDefault();
                    User = db.dbo_staff.Where(a => a.staff_id == SID).Select(a => a.staff_name).FirstOrDefault();
                    check = true;
                }
                else
                {
                    ErrMessage = "Password tidak sesuai";
                }
            }
            else
            {
                ErrMessage = "User tidak ditemukan";
                CheckUser = false;
            }
            return check;
        }

    }
}
