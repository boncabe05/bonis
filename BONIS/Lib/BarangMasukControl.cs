﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Lib
{
    public static class BarangMasukControl
    {
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        public static string ErrMessage { get; set; }

        private static DataTable DT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("IDCustomer", typeof(int));
            dt.Columns.Add("Supplier", typeof(string));
            dt.Columns.Add("Tanggal", typeof(string));
            return dt;
        }

        public static object loadBarangMasuk()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.BarangMasukControl.DT();
            var barangList = db.dbo_barang_masuk
                .Join(db.dbo_supplier, a => a.id_supplier, b => b.id_supplier, (a, b) =>
                new { a.id_masuk, b.id_supplier, b.nama_supplier, a.tanggal })
                .OrderByDescending(a => a.id_masuk).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_masuk, i.id_supplier, i.nama_supplier, i.tanggal);
            }
            return dt;
        }

        public static object searchBarangMasuk(string search)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = Lib.BarangMasukControl.DT();
            var barangList = db.dbo_barang_masuk
                .Join(db.dbo_supplier, a => a.id_supplier, b => b.id_supplier, (a, b) =>
                new { a.id_masuk, b.id_supplier, b.nama_supplier, a.tanggal })
                .Where(a => a.nama_supplier.Contains(search) || a.tanggal.Contains(search))
                .OrderByDescending(a => a.id_masuk).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_masuk, i.id_supplier, i.nama_supplier, i.tanggal);
            }
            return dt;
        }

        public static List<BarangMasukLItem> loadBarangMasukList(int idMasuk)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            var barangList = db.dbo_barang_masuk_LITEM
                .Where(a => a.id_barang_masuk == idMasuk)
                .Select(a => new BarangMasukLItem
                {
                    idBarangMasukLitem = a.id_LITEM_masuk,
                    idBarang = a.id_barang,
                    Jumlah = a.jumlah
                })
                .ToList();

            return barangList;
        }

        public static void loadSatuanList(DataGridViewComboBoxColumn cbo)
        {
            db = new AppData.InventoryEntities();
            var satuanList = db.dbo_satuan.OrderByDescending(a => a.id_satuan)
                .Select(a => new { a.id_satuan, a.nama_satuan }).ToList();
            cbo.DataSource = satuanList;
            cbo.ValueMember = "id_satuan";
            cbo.DisplayMember = "nama_satuan";
        }

        public static object loadBarang()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            var barangList = db.dbo_barang
                .OrderByDescending(a => a.id_barang)
                .Select(a => new {  a.id_barang, a.nama_barang })
                .ToList();

            return barangList;
        }

        public static object loadJumlah()
        {
            var jmlList = new List<TempJumlah>();
            for (int i = 1; i < 100; i++)
            {
                jmlList.Add(new TempJumlah { idjumlah = i, jumlah = i });
            }
            return jmlList;
        }

        public static bool checkStokBarang(int id, int stok)
        {
            db = new AppData.InventoryEntities();
            var maxStok = db.dbo_barang
                .Where(a => a.id_barang == id)
                .Select(a => a.stok).FirstOrDefault();
            bool res = (stok > maxStok) ? true : false;
            return res;
        }

        public static bool Crud(int method, 
            AppData.dbo_barang_masuk barang_masuk = null, 
            List<BarangMasukLItem> param_ = null, 
            string idBarangMasuk = null)
        {
            bool res = false;
            db = new AppData.InventoryEntities();
            switch (method)
            {
                case 0:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        db.dbo_barang_masuk.Add(barang_masuk);
                        db.SaveChanges();
                        int id_barang_masuk = barang_masuk.id_masuk;
                        try
                        {
                            AppData.InventoryEntities dbBarangMasuk = new AppData.InventoryEntities();
                            AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                            foreach (var param in param_)
                            {
                                var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                stokBarang.stok = stokBarang.stok + param.Jumlah;

                                dbBarangMasuk.dbo_barang_masuk_LITEM.Add(new AppData.dbo_barang_masuk_LITEM
                                {
                                    id_barang_masuk = id_barang_masuk,
                                    id_barang = param.idBarang,
                                    jumlah = param.Jumlah
                                });
                                try
                                {
                                    if (dbBarang.SaveChanges() > 0 && dbBarangMasuk.SaveChanges() > 0)
                                    {
                                        res = true;
                                    }
                                }
                                catch (Exception ee)
                                {
                                    ErrMessage += ee.Message;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage += ex.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                case 1:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        AppData.dbo_barang_masuk_LITEM BarangMasukLItem = new AppData.dbo_barang_masuk_LITEM();
                        int idMasuk = Convert.ToInt32(idBarangMasuk);
                        var barangMasuk = db.dbo_barang_masuk.Where(a => a.id_masuk == idMasuk).FirstOrDefault();
                        barangMasuk.id_supplier = barang_masuk.id_supplier;
                        barangMasuk.tanggal = barang_masuk.tanggal;
                        barangMasuk.user_id = barang_masuk.user_id;
                        db.SaveChanges();
                        int id_barang_masuk = Convert.ToInt32(idBarangMasuk);
                        try
                        {
                            AppData.InventoryEntities dbBarangMasuk = new AppData.InventoryEntities();
                            AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                            foreach (var param in param_)
                            {
                                var exist = db.dbo_barang_masuk_LITEM.Where(a => a.id_LITEM_masuk == param.idBarangMasukLitem).Any();
                                if (exist)
                                {
                                    if (!param.isDeleted)
                                    {
                                        var stokSebelumnya = dbBarang.dbo_barang_masuk_LITEM.Where(a => a.id_barang == param.idBarang && a.id_LITEM_masuk == param.idBarangMasukLitem).Select(a => a.jumlah).FirstOrDefault();
                                        var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                        if (param.Jumlah != stokSebelumnya)
                                        {
                                            var hasil = (stokBarang.stok - stokSebelumnya) + param.Jumlah;
                                            stokBarang.stok = hasil;
                                            try
                                            {
                                                if (dbBarang.SaveChanges() > 0)
                                                {
                                                    res = true;
                                                }
                                            }
                                            catch (Exception ee)
                                            {
                                                ErrMessage += ee.Message;
                                            }
                                        }
                                        BarangMasukLItem = dbBarangMasuk.dbo_barang_masuk_LITEM.Where(x => x.id_LITEM_masuk == param.idBarangMasukLitem && x.id_barang_masuk == id_barang_masuk).FirstOrDefault();
                                        BarangMasukLItem.id_barang = param.idBarang;
                                        BarangMasukLItem.jumlah = param.Jumlah;
                                        try
                                        {
                                            dbBarangMasuk.SaveChanges();
                                        }
                                        catch (Exception ee)
                                        {
                                            ErrMessage += ee.Message;
                                        }

                                    }
                                    else
                                    {

                                        var stokSebelumnya = dbBarang.dbo_barang_masuk_LITEM.Where(a => a.id_barang == param.idBarang && a.id_LITEM_masuk == param.idBarangMasukLitem).Select(a => a.jumlah).FirstOrDefault();
                                        var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                        var hasil = stokBarang.stok - stokSebelumnya;
                                        stokBarang.stok = hasil;
                                        BarangMasukLItem = dbBarangMasuk.dbo_barang_masuk_LITEM.Where(x => x.id_LITEM_masuk == param.idBarangMasukLitem).FirstOrDefault();
                                        dbBarangMasuk.dbo_barang_masuk_LITEM.Remove(BarangMasukLItem);
                                        if (dbBarang.SaveChanges() > 0 && dbBarangMasuk.SaveChanges() > 0)
                                        {
                                            res = true;
                                        }
                                    }
                                }
                                else
                                {
                                    var stokBarang = dbBarang.dbo_barang.Where(a => a.id_barang == param.idBarang).FirstOrDefault();
                                    stokBarang.stok = stokBarang.stok + param.Jumlah;
                                    dbBarangMasuk.dbo_barang_masuk_LITEM.Add(new AppData.dbo_barang_masuk_LITEM
                                    {
                                        id_barang_masuk = id_barang_masuk,
                                        id_barang = param.idBarang,
                                        jumlah = param.Jumlah
                                    });
                                    if (dbBarang.SaveChanges() > 0 && dbBarangMasuk.SaveChanges() > 0)
                                    {
                                        res = true;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage += ex.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                case 2:
                    try
                    {
                        db = new AppData.InventoryEntities();
                        AppData.InventoryEntities dbBarangMasuk = new AppData.InventoryEntities();
                        AppData.InventoryEntities dbBarang = new AppData.InventoryEntities();
                        int idBarang_Masuk = Convert.ToInt32(idBarangMasuk);
                        var del = db.dbo_barang_masuk
                            .Where(a => a.id_masuk == idBarang_Masuk)
                            .FirstOrDefault();
                        var barangMasukList = dbBarangMasuk.dbo_barang_masuk_LITEM
                            .Where(a => a.id_barang_masuk == del.id_masuk)
                            .ToList();
                        foreach (var barangList in barangMasukList)
                        {
                            var barang = dbBarang.dbo_barang.Where(a => a.id_barang == barangList.id_barang).FirstOrDefault();
                            barang.stok = barang.stok - barangList.jumlah;
                        }

                        dbBarangMasuk.dbo_barang_masuk_LITEM
                            .RemoveRange(dbBarangMasuk.dbo_barang_masuk_LITEM
                            .Where(a => a.id_barang_masuk == idBarang_Masuk));
                        db.dbo_barang_masuk.Remove(del);
                        try
                        {
                            if (db.SaveChanges() > 0
                                && dbBarang.SaveChanges() > 0
                                && dbBarangMasuk.SaveChanges() > 0)
                            {
                                res = true;
                            }
                        }
                        catch (Exception ee)
                        {
                            ErrMessage += ee.Message;

                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += ex.Message;
                    }
                    break;
                default:
                    ErrMessage += "Tidak memilih apapun";
                    break;
            }
            return res;
        }
    }

    public class BarangMasukLItem
    {
        public int idBarangMasuk { get; set; }
        public int idBarangMasukLitem { get; set; }
        public int idBarang { get; set; }
        public int Jumlah { get; set; }
        public bool isDeleted { get; set; }
    }
}
