﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BONIS.Lib
{
    public static class StaffControl
    {
        private static string ErrMessage { get; set; }
        private static string Password_ { get; set; }
        private static bool res = false;
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        private static DataTable dt()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Staff", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("Phone", typeof(string));
            dt.Columns.Add("UserID", typeof(int));
            dt.Columns.Add("Username", typeof(string));

            return dt;
        }
        public static object loadStaff()
        {
            DataTable dt = Lib.StaffControl.dt();
            var staffList = db.dbo_staff.Join(db.dbo_user, a => a.user_id, b => b.user_id, (a, b) =>
                new { a.staff_id, a.staff_name, a.email, a.phone, b.user_id, b.username })
                .OrderByDescending(a => a.staff_id).ToList();
            foreach (var i in staffList)
            {
                dt.Rows.Add(i.staff_id, i.staff_name, i.email, i.phone, i.user_id, i.username);
            }

            return dt;
        }

        public static object searchStaff(string search)
        {
            DataTable dt = Lib.StaffControl.dt();
            if(search != "")
            {
                var staffList = db.dbo_staff
                .Join(db.dbo_user, a => a.user_id, b => b.user_id, (a, b) =>
                new { a.staff_id, a.staff_name, a.email, a.phone, b.user_id, b.username })
                .Where(a => a.staff_name.Contains(search) || a.phone.Contains(search) || a.email.Contains(search) || a.username.Contains(search))
                .OrderByDescending(a => a.staff_id).ToList();
                foreach (var i in staffList)
                {
                    dt.Rows.Add(i.staff_id, i.staff_name, i.email, i.phone, i.user_id, i.username);
                }
            }
            else
            {
                var staffList = db.dbo_staff
                .Join(db.dbo_user, a => a.user_id, b => b.user_id, (a, b) =>
                new { a.staff_id, a.staff_name, a.email, a.phone, b.user_id, b.username })
                .OrderByDescending(a => a.staff_id).ToList();
                foreach (var i in staffList)
                {
                    dt.Rows.Add(i.staff_id, i.staff_name, i.email, i.phone, i.user_id, i.username);
                }
            }
            
            return dt;
        }

        public static bool CRUD(int method, StaffUser staff, bool changePassword = false)
        {
            db = new AppData.InventoryEntities();
            AppData.InventoryEntities dbUser = new AppData.InventoryEntities();
            PasswordHash PassHash = new PasswordHash();
            switch (method)
            {
                case 0:
                    Password_ = PassHash.HashPassword(staff.Password);
                    var userAdd = dbUser.dbo_user.Add(new AppData.dbo_user
                    {
                        username = staff.Username,
                        password = Password_
                    });
                    try
                    {
                        if (dbUser.SaveChanges() > 0)
                        {
                            var userID = userAdd.user_id;

                            db.dbo_staff.Add(new AppData.dbo_staff
                            {
                                staff_name = staff.StaffName,
                                email = staff.StaffEmail,
                                phone = staff.StaffPhone,
                                user_id = userID
                            });
                            try
                            {
                                if (db.SaveChanges() > 0)
                                {
                                    res = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrMessage += " Add User Error : " + ex.Message;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Add Staff Error : " + ex.Message;
                    }
                    break;
                case 1:
                    var Staff_ = db.dbo_staff.Where(a => a.staff_id == staff.StaffID).FirstOrDefault();
                    Staff_.staff_name = staff.StaffName;
                    Staff_.phone = staff.StaffPhone;
                    Staff_.email = staff.StaffEmail;
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            if (changePassword)
                            {
                                Password_ = PassHash.HashPassword(staff.Password);
                                var userEdit = dbUser.dbo_user.Where(a => a.user_id == staff.UserID).FirstOrDefault();
                                userEdit.username = staff.Username;
                                userEdit.password = Password_;
                                try
                                {
                                    if (dbUser.SaveChanges() > 0)
                                    {
                                        res = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrMessage += " Change Password Error : " + ex.Message;
                                }
                            }
                            else
                            {
                                res = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Update Staff error : " + ex.Message;
                    }

                    break;
                case 2:
                    var removeStaff = db.dbo_staff.Where(a => a.staff_id == staff.StaffID).FirstOrDefault();
                    var removeUser = dbUser.dbo_user.Where(a => a.user_id == staff.UserID).FirstOrDefault();
                    try
                    {
                        db.dbo_staff.Remove(removeStaff);
                        dbUser.dbo_user.Remove(removeUser);
                        if (db.SaveChanges() > 0 && dbUser.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Remove user error " + ex.Message;
                    }
                    break;
                default:
                    res = false;
                    ErrMessage += " No Data ";
                    break;
            }
            return res;
        }

        public static bool checkUsername(string username)
        {
            db = new AppData.InventoryEntities();
            bool result = db.dbo_user.Where(a => a.username == username).Any();
            return result;
        }

        public static bool checkUsernameEdit(string username, int userID)
        {
            db = new AppData.InventoryEntities();
            bool result = db.dbo_user.Where(a => a.user_id != userID && a.username == username).Any();
            return result;
        }
    }

    public class StaffUser
    {
        public int StaffID { get; set; }
        public string StaffName { get; set; }
        public string StaffPhone { get; set; }
        public string StaffEmail { get; set; }
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
