﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SQLite;
using System.Data.SqlClient;

namespace BONIS
{
    public class Connection
    {
        
        //private static string ConnStrMdf = Properties.Settings.Default.InventoryConnectionString;
        private static string ConnStrSqlite = Properties.Settings.Default.InventorySqliteConnectionString;
        public static string ErrMsg = "";
        public static DbList UseDB = DbList.None;

        public enum DbList
        {
            None,
            Sqlite,
            MSSql
        };

        public static bool CheckCon()
        {
            bool check = false;
            //SqlConnection sqlcon = new SqlConnection(ConnStrMdf);
            SQLiteConnection sqlitecon = new SQLiteConnection(ConnStrSqlite);
            try
            {
                sqlitecon.Open();
                sqlitecon.Close();
                UseDB = DbList.Sqlite;
                check = true;
            }
            catch (Exception e)
            {
                check = false;
                ErrMsg += e.Message;
            }
            return check;
        }
    }
}
