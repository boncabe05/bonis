﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BONIS.Lib
{
    public static class UsersControl
    {
        private static string ErrMessage { get; set; }
        private static string Password_ { get; set; }
        private static bool res = false;
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();

        private static DataTable DT()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Username", typeof(string));
            dt.Columns.Add("Nama Staff", typeof(string));

            return dt;
        }
        public static object loadUser()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = DT();
            var userList = db.dbo_user.Join(db.dbo_staff, a => a.user_id, b => b.user_id, (a, b) =>
                new { a.user_id, a.username, a.password, b.staff_name })
                .OrderByDescending(a => a.user_id).ToList();
            foreach (var i in userList)
            {
                dt.Rows.Add(i.user_id, i.username, i.staff_name);
            }
            return dt;
        }

        public static object searchUser(string search)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = DT();
            if (search != "")
            {
                var userList = db.dbo_user
                    .Join(db.dbo_staff, a => a.user_id, b => b.user_id, (a, b) =>
                    new { a.user_id, a.username, a.password, b.staff_name })
                    .Where(a => a.username.Contains(search) || a.staff_name.Contains(search))
                    .OrderByDescending(a => a.user_id).ToList();
                foreach (var i in userList)
                {
                    dt.Rows.Add(i.user_id, i.username, i.staff_name);
                }
            }
            else
            {
                var userList = db.dbo_user
                    .Join(db.dbo_staff, a => a.user_id, b => b.user_id, (a, b) =>
                    new { a.user_id, a.username, a.password, b.staff_name })
                    .OrderByDescending(a => a.user_id).ToList();
                foreach (var i in userList)
                {
                    dt.Rows.Add(i.user_id, i.username, i.staff_name);
                }
            }
            
            return dt;
        }

        public static bool checkUsernameEdit(string username, int userID)
        {
            db = new AppData.InventoryEntities();
            bool result = db.dbo_user.Where(a => a.user_id != userID && a.username == username).Any();
            return result;
        }

        public static bool CRUD(int method, AppData.dbo_user user, bool changePassword = false)
        {
            db = new AppData.InventoryEntities();
            PasswordHash PassHash = new PasswordHash();
            switch (method)
            {
                case 1:
                    var User_ = db.dbo_user.Where(a => a.user_id == user.user_id).FirstOrDefault();
                    User_.username = user.username;
                    
                    if (changePassword)
                    {
                        Password_ = PassHash.HashPassword(user.password);
                        User_.password = Password_;
                        try
                        {
                            if (db.SaveChanges() > 0)
                            {
                                res = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrMessage += " Change Password Error : " + ex.Message;
                        }
                    }
                    else
                    {
                        try
                        {
                            if(db.SaveChanges() > 0)
                            {
                                res = true;
                            }
                        }catch(Exception ex)
                        {
                            ErrMessage += " Edit user error : " + ex.Message;
                        }
                        
                    }
                    break;
                default:
                    res = false;
                    ErrMessage += " No Data ";
                    break;
            }
            return res;
        }

    }
}
