﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BONIS.Lib
{
    public static class BarangControl
    {
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        public static string ErrMessage { get; set; }

        public static object loadBarang()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Barang", typeof(string));
            dt.Columns.Add("Stok Barang", typeof(int));
            dt.Columns.Add("Satuan", typeof(string));
            var barangList = db.dbo_barang
                .Join(db.dbo_satuan, a => a.id_satuan, b => b.id_satuan, (a, b) =>
                new { a.id_barang, a.nama_barang, a.stok, b.nama_satuan })
                .OrderByDescending(a => a.id_barang).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_barang, i.nama_barang, i.stok, i.nama_satuan);
            }
            return dt;
        }

        public static object searchBarang(string search)
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Barang", typeof(string));
            dt.Columns.Add("Stok Barang", typeof(int));
            dt.Columns.Add("Satuan", typeof(string));
            var barangList = db.dbo_barang
                .Join(db.dbo_satuan, a => a.id_satuan, b => b.id_satuan, (a, b) =>
                new { a.id_barang, a.nama_barang, a.stok, b.nama_satuan })
                .Where(x => x.nama_barang.Contains(search))
                .OrderByDescending(a => a.id_barang).ToArray();
            foreach (var i in barangList)
            {
                dt.Rows.Add(i.id_barang, i.nama_barang, i.stok, i.nama_satuan);
            }
            return dt;
        }

        public static bool Crud(int method, BarangClass param)
        {
            bool res = false;
            db = new AppData.InventoryEntities();
            switch (method)
            {
                case 0:
                    try
                    {
                        db.dbo_barang.Add(new AppData.dbo_barang
                        {
                            nama_barang = param.NamaBarang,
                            stok = param.StokBarang,
                            id_satuan = param.ID_Satuan
                        });
                        db.SaveChanges();
                        res = true;
                    }catch(Exception ex)
                    {
                        ErrMessage = ex.Message;
                    }
                    break;
                case 1:
                    AppData.dbo_barang barang;
                    barang = db.dbo_barang.Where(a => a.id_barang == param.ID).FirstOrDefault<AppData.dbo_barang>();
                    if(barang != null)
                    {
                        barang.nama_barang = param.NamaBarang;
                        barang.stok = param.StokBarang;
                        barang.id_satuan = param.ID_Satuan;
                    }
                    try
                    {
                        db.Entry(barang).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        res = true;
                    }
                    catch (Exception ex)
                    {
                        ErrMessage = ex.Message;
                    }
                    break;
                case 2:
                    try
                    {
                        var del = db.dbo_barang.Where(a => a.id_barang == param.ID).First();
                        db.dbo_barang.Remove(del);
                        db.SaveChanges();
                        res = true;
                    }
                    catch (Exception ex)
                    {
                        ErrMessage = ex.Message;
                    }
                    break;
                default:
                    ErrMessage = "Tidak memilih apapun";
                    break;
            }
            return res;
        }
    }

    public class BarangClass
    {
        public int ID { get; set; }
        public string NamaBarang { get; set; }
        public int StokBarang { get; set; }
        public string Satuan { get; set; }
        public int ID_Satuan { get; set; }
    }
}
