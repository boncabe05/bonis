﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Lib
{
    public static class CustomerControl
    {
        private static string ErrMessage { get; set; }
        private static bool res = false;
        private static AppData.InventoryEntities db = new AppData.InventoryEntities();
        private static DataTable dt()
        {
            db = new AppData.InventoryEntities();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nama Customer", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("Phone", typeof(string));
            dt.Columns.Add("Alamat", typeof(string));
            return dt;
        }
        public static object loadCustomer()
        {
            DataTable dt = Lib.CustomerControl.dt();
            var customerList = db.dbo_customer
                .OrderByDescending(a => a.id_customer).ToList();
            foreach (var i in customerList)
            {
                dt.Rows.Add(i.id_customer, i.nama_customer, i.email, i.phone, i.alamat);
            }

            return dt;
        }

        public static object searchCustomer(string search)
        {
            DataTable dt = Lib.CustomerControl.dt();
            if (search != "")
            {
                var customerList = db.dbo_customer
                .Where(a => a.nama_customer.Contains(search) || a.phone.Contains(search) || a.email.Contains(search) || a.alamat.Contains(search))
                .OrderByDescending(a => a.id_customer).ToList();
                foreach (var i in customerList)
                {
                    dt.Rows.Add(i.id_customer, i.nama_customer, i.email, i.phone, i.alamat);
                }
            }
            else
            {
                var customerList = db.dbo_customer
                .OrderByDescending(a => a.id_customer).ToList();
                foreach (var i in customerList)
                {
                    dt.Rows.Add(i.id_customer, i.nama_customer, i.email, i.phone, i.alamat);
                }
            }

            return dt;
        }

        public static bool CRUD(int method, AppData.dbo_customer customer)
        {
            db = new AppData.InventoryEntities();
            PasswordHash PassHash = new PasswordHash();
            switch (method)
            {
                case 0:
                    db.dbo_customer.Add(new AppData.dbo_customer
                    {
                        nama_customer = customer.nama_customer,
                        email = customer.email,
                        phone = customer.phone,
                        alamat = customer.alamat
                    });
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Add Customer Error : " + ex.Message;
                    }
                    break;
                case 1:
                    var Customer_ = db.dbo_customer.Where(a => a.id_customer == customer.id_customer).FirstOrDefault();
                    Customer_.nama_customer = customer.nama_customer;
                    Customer_.phone = customer.phone;
                    Customer_.email = customer.email;
                    Customer_.alamat = customer.alamat;
                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Update Customer error : " + ex.Message;
                    }

                    break;
                case 2:
                    var removeCustomer = db.dbo_customer.Where(a => a.id_customer == customer.id_customer).FirstOrDefault();
                    try
                    {
                        db.dbo_customer.Remove(removeCustomer);
                        if (db.SaveChanges() > 0)
                        {
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMessage += " Remove user error " + ex.Message;
                    }
                    break;
                default:
                    res = false;
                    ErrMessage += " No Data ";
                    break;
            }
            return res;
        }

        public static object loadCustomerList()
        {
            db = new AppData.InventoryEntities();
            var customerList = db.dbo_customer
                .OrderByDescending(a => a.id_customer)
                .Select(a => new { a.id_customer, a.nama_customer})
                .ToList();

            return customerList;
        }
    }
}
