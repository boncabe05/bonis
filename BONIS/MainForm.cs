﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS
{
    public partial class MainForm : Form
    {
        #region shadowborder
        //Start shadow
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        [DllImport("dwmapi.dll")]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS pMarInset);

        [DllImport("dwmapi.dll")]
        public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref int attrValue, int attrSize);

        [DllImport("dwmapi.dll")]
        public static extern int DwmIsCompositionEnabled(ref int pfEnabled);

        private bool m_aeroEnabled;
        private const int CS_DROPSHADOW = 0x60C2F0;
        private const int WM_NCPAINT = 0x0085;
        private const int WM_ACTIVATEAPP = 0x001C;

        public struct MARGINS
        {
            public int leftWidth;
            public int rightWidth;
            public int topHeight;
            public int bottomHeight;
        }

        private const int WM_NCHITTEST = 0x84;
        private const int HTCLIENT = 0x1;
        private const int HTCAPTION = 0x2;

        protected override CreateParams CreateParams
        {
            get
            {
                m_aeroEnabled = CheckAeroEnabled();

                CreateParams cp = base.CreateParams;
                if (!m_aeroEnabled)
                    cp.ClassStyle |= CS_DROPSHADOW;

                return cp;
            }
        }

        private bool CheckAeroEnabled()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                int enabled = 0;
                DwmIsCompositionEnabled(ref enabled);
                return (enabled == 1) ? true : false;
            }
            return false;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCPAINT:
                    if (m_aeroEnabled)
                    {
                        var v = 2;
                        DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        MARGINS margins = new MARGINS()
                        {
                            bottomHeight = 1,
                            leftWidth = 1,
                            rightWidth = 1,
                            topHeight = 1
                        };
                        DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
                case WM_NCHITTEST:
                    m.Result = (IntPtr)(HT_CAPTION);
                    break;
                default:
                    if (m_aeroEnabled)
                    {
                        var v = 2;
                        DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        MARGINS margins = new MARGINS()
                        {
                            bottomHeight = 1,
                            leftWidth = 1,
                            rightWidth = 1,
                            topHeight = 1
                        };
                        DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
            }
            base.WndProc(ref m);

            if (m.Msg == WM_NCHITTEST && (int)m.Result == HTCLIENT)
                m.Result = (IntPtr)HTCAPTION;

        }
        #endregion

        private Timer _timer, timer2;
        public MainForm()
        {
            InitializeComponent();
            Lib.NotificationMessage.Notification = "";

        }

        private async void loadFirst()
        {

            Loading load_ = new Loading();
            //this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                
            }), TaskScheduler.FromCurrentSynchronizationContext());
            //load_ = (Loading)Application.OpenForms["Loading"];
            //load_.Close();
        }

        void checkLogin(object sender, EventArgs args) { 
            if (Lib.ManageUser.RID > 0){
                checkUserID();
            }
            else
            {
                _timer.Dispose();
                Login frmLogin = new Login();
                this.BeginInvoke(new Action(() => frmLogin.ShowDialog(this)));
                //frmLogin.ShowDialog(this);
                //frmLogin.Dispose();
                if (frmLogin.DialogResult == DialogResult.OK)
                {
                    checkUserID();
                }
            }
        }

        void checkUserID()
        {
            BONIS.Modules.MenuAdmin mnAdmin = new BONIS.Modules.MenuAdmin();
            switch (Lib.ManageUser.RID)
            {
                case 1:
                    mnAdmin.Dock = DockStyle.Left;
                    SidePanel.Controls.Add(mnAdmin);
                    break;
                case 2:
                    mnAdmin.Dock = DockStyle.Left;
                    SidePanel.Controls.Add(mnAdmin);
                    break;
                default:
                    _timer.Dispose();
                    break;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            BONIS.Modules.MenuAdmin mnAdmin = new BONIS.Modules.MenuAdmin();
            mnAdmin.Dock = DockStyle.Left;
            SidePanel.Controls.Add(mnAdmin);
            Modules.MasterControl mst = new Modules.MasterControl();
            ContentPanel.Controls.Clear();
            ContentPanel.Controls.Add(mst);

            mnAdmin.Dock = DockStyle.Left;
            SidePanel.Controls.Add(mnAdmin);
            _timer = new Timer();
            _timer.Interval = 200;
            _timer.Start();
            _timer.Tick += new EventHandler(checkLogin);
            timer1.Start();

            timer2 = new Timer();
            timer2.Interval = 100;
            timer2.Start();
            timer2.Tick += new EventHandler(updateNotif);
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }else 
            {
                this.WindowState = FormWindowState.Normal;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Anda yakin ingin menutup aplikasi ini ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                return;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var date = DateTime.Now.ToLocalTime();
            lblTime.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm:ss");
            
        }

        private void updateNotif(object sender, EventArgs e)
        {
            //lblNotification.Text = Lib.NotificationMessage.Notification;
        }
        
        
        /*moveable panel */
        #region moveablepanel

        int panelMoveM, MValX, MValY;

        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            panelMoveM = 0;
        }

        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (panelMoveM == 1)
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
            }
        }

        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            panelMoveM = 1;
            MValX = e.X;
            MValY = e.Y;
        }

        private void MainPanel_MouseUp(object sender, MouseEventArgs e)
        {
            panelMoveM = 0;
        }

        private void MainPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (panelMoveM == 1)
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
            }
        }

        private void MainPanel_MouseDown(object sender, MouseEventArgs e)
        {
            panelMoveM = 1;
            MValX = e.X;
            MValY = e.Y;
        }
        #endregion
    }
}
