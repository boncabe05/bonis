﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlSupplier : UserControl
    {
        bool flagAdd, flagEdit, flagDelete = false;

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Supplier");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Supplier");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Supplier");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Supplier");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Supplier");
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvSupplier.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvSupplier.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                loadTooltip();
                clear();
                supplierLoad();
                disabled();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        public ControlSupplier()
        {
            InitializeComponent();
            

        }

        private void supplierLoad()
        {
            dgvSupplier.DataSource = Lib.SupplierControl.loadSupplier();
            dgvSupplier.Columns[0].Visible = false;
            this.dgvSupplier.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvSupplier.Refresh();
        }

        private void supplierSearch(string search)
        {
            dgvSupplier.DataSource = Lib.SupplierControl.searchSupplier(search);
            dgvSupplier.Columns[0].Visible = false;
            this.dgvSupplier.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvSupplier.Refresh();
        }

        private void enabled()
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            txtNamaSupplier.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtAlamat.Enabled = true;
        }

        private void disabled()
        {
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            txtNamaSupplier.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtAlamat.Enabled = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            supplierSearch(txtSearch.Text);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string supplierName_ = txtNamaSupplier.Text;
            string supplierPhone_ = txtPhone.Text;
            string supplierEmail_ = txtEmail.Text;
            string supplierId_ = txtSupplierID.Text;
            string supplierAlamat_ = txtAlamat.Text;
            if(supplierName_ != "")
            {
                AppData.dbo_supplier supplier = new AppData.dbo_supplier();
                if (flagAdd)
                {
                    supplier.nama_supplier = supplierName_;
                    supplier.phone = supplierPhone_;
                    supplier.email = supplierEmail_;
                    supplier.alamat = supplierAlamat_;
                    if (Lib.SupplierControl.CRUD(0, supplier))
                    {

                        Lib.NotificationMessage.Notification = "Berhasil menambahkan supplier";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal menambahkan supplier";
                    }
                    supplierLoad();
                    clear();
                    disabled();

                }
                if (flagEdit)
                {
                    if (supplierId_ != "")
                    {
                        supplier.nama_supplier = supplierName_;
                        supplier.phone = supplierPhone_;
                        supplier.email = supplierEmail_;
                        supplier.alamat = supplierAlamat_;
                        supplier.id_supplier = Convert.ToInt32(supplierId_);
                        if (Lib.SupplierControl.CRUD(1, supplier))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil mengedit supplier";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal mengedit supplier";
                        }
                        supplierLoad();
                        clear();
                        disabled();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih supplier terlebih dahulu", "Konfirmasi");
                        dgvSupplier.Focus();
                        return;
                    }
                }
                if (flagDelete)
                {
                    if (supplierId_ != "")
                    {
                        supplier.nama_supplier = supplierName_;
                        supplier.phone = supplierPhone_;
                        supplier.email = supplierEmail_;
                        supplier.alamat = supplierAlamat_;
                        supplier.id_supplier = Convert.ToInt32(supplierId_);
                        if (Lib.SupplierControl.CRUD(2, supplier))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil menghapus supplier";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal menghapus supplier";
                        }
                        supplierLoad();
                        clear();
                        disabled();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih supplier terlebih dahulu", "Konfirmasi");
                        dgvSupplier.Focus();
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Nama Supplier tidak boleh kosong", "Konfirmasi");
                txtNamaSupplier.Focus();
                return;
            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            clear();
            supplierLoad();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtSupplierID.Text != "")
            {
                flagDelete = true;
                enabled();
            }
            else
            {
                MessageBox.Show("Harap pilih supplier terlebih dahulu!", "Konfirmasi");
                dgvSupplier.Focus();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void ControlSupplier_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void dgvSupplier_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtSupplierID.Text = dgvSupplier[0, e.RowIndex].Value.ToString();
                txtNamaSupplier.Text = dgvSupplier[1, e.RowIndex].Value.ToString();
                txtPhone.Text = dgvSupplier[2, e.RowIndex].Value.ToString();
                txtEmail.Text = dgvSupplier[3, e.RowIndex].Value.ToString();
                txtAlamat.Text = dgvSupplier[4, e.RowIndex].Value.ToString();

            }
        }

        private void clear()
        {
            flagAdd = false; flagEdit = false; flagDelete = false;
            txtNamaSupplier.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtSearch.Text = "";
            txtSupplierID.Text = "";
            txtAlamat.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                clear();
                disabled();
                supplierLoad();
                
            }
            else
            {
                return;
            }
        }
    }
}
