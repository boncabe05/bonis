﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace BONIS.Modules
{
    public partial class ControlBarangMasuk : UserControl
    {
        private bool flagAdd, flagEdit, flagDelete = false;
        bool IDSama = false;

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Barang Masuk");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Barang Masuk");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data Masuk");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Barang Masuk");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Barang Masuk");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Barang Masuk");
        }
        public ControlBarangMasuk()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvBarangMasuk.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvTemp.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                dgvTemp.EnableHeadersVisualStyles = false;
                loadTooltip();
                clear();
                disabled();
                tempLoad();
                cboLoad();
                BarangMasukLoad();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void ControlBarangMasuk_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void tempLoad()
        {
            dgvCboNamaBarang.DataSource = Lib.BarangMasukControl.loadBarang();
            dgvCboNamaBarang.DisplayMember = "nama_barang";
            dgvCboNamaBarang.ValueMember = "id_barang";
            dgvCboJumlah.DataSource = Lib.BarangMasukControl.loadJumlah();
            dgvCboJumlah.DisplayMember = "idjumlah";
            dgvCboJumlah.ValueMember = "jumlah";

        }

        private void cboLoad()
        {
            cboSupplier.DataSource = Lib.SupplierControl.loadSupplierList();
            cboSupplier.DisplayMember = "nama_supplier";
            cboSupplier.ValueMember = "id_supplier";
        }

        private void BarangMasukLoad()
        {
            dgvBarangMasuk.DataSource = Lib.BarangMasukControl.loadBarangMasuk();
            dgvBarangMasuk.Columns[0].Visible = false;
            dgvBarangMasuk.Columns[1].Visible = false;
            dgvBarangMasuk.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvBarangMasuk.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void clear()
        {
            dgvTemp.Rows.Clear();
            txtIDBarangMasuk.Text = "";
            if(dgvTemp.Columns.Contains("cImg"))
            {
                dgvTemp.Columns.Remove("cImg");
            }
        }

        private void enabled()
        {
            cboSupplier.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            dgvTemp.Enabled = true;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dgvTemp.RowsDefaultCellStyle.BackColor = Color.White;
            dgvTemp.DefaultCellStyle.BackColor = SystemColors.Window;
            dgvTemp.DefaultCellStyle.ForeColor = SystemColors.ControlText;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
            dgvTemp.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ReadOnly = false;
            dgvTemp.EnableHeadersVisualStyles = true;
        }

        private void disabled()
        {
            cboSupplier.Enabled = false;
            dgvTemp.Enabled = false;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = Color.Gray;
            dgvTemp.RowsDefaultCellStyle.BackColor = Color.Gray;
            dgvTemp.DefaultCellStyle.BackColor = SystemColors.GrayText;
            dgvTemp.DefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
            dgvTemp.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ReadOnly = true;
            dgvTemp.EnableHeadersVisualStyles = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            dgvBarangMasuk.DataSource = Lib.BarangMasukControl.loadBarangMasuk();
            dgvBarangMasuk.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!IDSama)
            {
                List<Lib.BarangMasukLItem> items = new List<Lib.BarangMasukLItem>();
                AppData.dbo_barang_masuk barang_masuk = new AppData.dbo_barang_masuk();
                Label idUser = (this.Parent.Parent.Parent as MainForm).lblIDUser as Label;
                int id_user = Convert.ToInt32(idUser.Text);
                if (flagAdd)
                {
                    barang_masuk.user_id = id_user;
                    barang_masuk.id_supplier = Convert.ToInt32(cboSupplier.SelectedValue.ToString());
                    barang_masuk.tanggal = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    foreach (DataGridViewRow item in dgvTemp.Rows)
                    {
                        if (null != item && null != item.Cells[1].Value)
                        {
                            items.Add(new Lib.BarangMasukLItem
                            {
                                idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                            });
                        }
                    }

                    if (Lib.BarangMasukControl.Crud(0, barang_masuk, items))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Menambahkan Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Menambahkan Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    BarangMasukLoad();
                    flagAdd = false;
                }
                if (flagEdit)
                {
                    barang_masuk.user_id = id_user;
                    barang_masuk.id_supplier = Convert.ToInt32(cboSupplier.SelectedValue.ToString());
                    barang_masuk.tanggal = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    foreach (DataGridViewRow item in dgvTemp.Rows)
                    {
                        if (null != item && null != item.Cells[1].Value)
                        {
                            if (item.Visible == true)
                            {
                                int idMasukLItem = (item != null && item.Cells[0].Value != null) ? Convert.ToInt32(item.Cells[0].Value.ToString()) : 0;
                                items.Add(new Lib.BarangMasukLItem
                                {
                                    idBarangMasukLitem = idMasukLItem,
                                    idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                    Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                                    idBarangMasuk = Convert.ToInt32(txtIDBarangMasuk.Text),
                                    isDeleted = false
                                });

                            }
                            else
                            {
                                int idMasukLItem = (item != null && item.Cells[0].Value != null) ? Convert.ToInt32(item.Cells[0].Value.ToString()) : 0;
                                items.Add(new Lib.BarangMasukLItem
                                {
                                    idBarangMasukLitem = Convert.ToInt32(item.Cells[0].Value.ToString()),
                                    idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                    Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                                    idBarangMasuk = Convert.ToInt32(txtIDBarangMasuk.Text),
                                    isDeleted = true
                                });
                            }
                        }
                    }

                    if (Lib.BarangMasukControl.Crud(1, barang_masuk, items, txtIDBarangMasuk.Text))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Mengedit Data Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Mengedit Data Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    BarangMasukLoad();
                    flagEdit = false;
                }
                if (flagDelete)
                {
                    if (Lib.BarangMasukControl.Crud(2, barang_masuk, items, txtIDBarangMasuk.Text))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Menghapus Data Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Menghapus Data Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    BarangMasukLoad();
                    flagEdit = false;
                }
            }
            else
            {
                if (IDSama) { MessageBox.Show("Barang tidak boleh sama"); return; }
            }
        }

        private void dgvBarang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgvBarangMasuk[0, e.RowIndex].Value != null && dgvBarangMasuk[0, e.RowIndex].Value.ToString() != "" )
                {
                    txtIDBarangMasuk.Text = dgvBarangMasuk[0, e.RowIndex].Value.ToString();
                    cboSupplier.SelectedValue = dgvBarangMasuk[1, e.RowIndex].Value;
                    int idmasuk = Convert.ToInt32(txtIDBarangMasuk.Text);
                    loadDataLitem(idmasuk);
                }
            }
        }

        private void loadDataLitem(int idMasuk)
        {
            dgvTemp.Rows.Clear();
            var items = Lib.BarangMasukControl.loadBarangMasukList(idMasuk);
            int i = 0;
            if (!dgvTemp.Columns.Contains("cImg"))
            {
                DataGridViewImageColumn ic = new DataGridViewImageColumn();
                ic.HeaderText = "Delete";
                ic.Image = null;
                ic.Name = "cImg";
                ic.Width = 100;
                dgvTemp.Columns.Add(ic);
            }
            foreach (var item in items)
            {
                dgvTemp.Rows.Add();
                dgvTemp.Rows[i].Cells[0].Value = item.idBarangMasukLitem;
                dgvTemp.Rows[i].Cells[1].Value = item.idBarang;
                dgvTemp.Rows[i].Cells[2].Value = item.Jumlah;
                dgvTemp.Rows[i].Cells[3].Value = (System.Drawing.Image)Properties.Resources.icons8_delete_16;
                i++;
            }

        }

        private void dgvTemp_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                
                if (dgvTemp.Columns[1].Index == e.ColumnIndex)
                {
                    bool areEqual = false;
                    if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                    {
                        List<string> sameID = new List<string>();
                        if (dgvTemp.RowCount > 2)
                        {

                            foreach (DataGridViewRow row in dgvTemp.Rows)
                            {
                                if (null != row && null != row.Cells[1].Value)
                                {
                                    string idBarang1 = row.Cells[1].Value.ToString();
                                    sameID.Add(idBarang1);

                                }
                            }
                            areEqual = sameID.GroupBy(x => x).Any(g => g.Count() > 1);
                            if (areEqual)
                            {
                                MessageBox.Show("Tidak boleh memilih barang yang sama!", "Konfirmasi");
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                IDSama = true;
                            }
                            else
                            {
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                IDSama = false;
                            }
                        }
                    }
                }
            }
        }

        private void dgvTemp_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvTemp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTemp.Columns.Contains("cImg"))
            {
                if (dgvTemp.Columns[3].Index == e.ColumnIndex)
                {
                    var senderGrid = (DataGridView)sender;
                    var column = senderGrid.Columns[3];
                    if (e.RowIndex >= 0)
                    {
                        dgvTemp.Rows[e.RowIndex].Visible = false;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            enabled();
            flagDelete = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string search_ = txtSearch.Text;
            dgvBarangMasuk.DataSource = Lib.BarangMasukControl.searchBarangMasuk(search_);
            dgvBarangMasuk.Columns[0].Visible = false;
            dgvBarangMasuk.Columns[1].Visible = false;
            dgvBarangMasuk.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvBarangMasuk.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
                txtSearch.Focus();
            }
        }

        private void dgvTemp_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgvTemp.Columns[1].Index == e.ColumnIndex)
                {
                    bool areEqual = false;
                    if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                    {
                        List<string> sameID = new List<string>();
                        if (dgvTemp.RowCount > 2)
                        {

                            foreach (DataGridViewRow row in dgvTemp.Rows)
                            {
                                if (null != row && null != row.Cells[1].Value)
                                {
                                    string idBarang1 = row.Cells[1].Value.ToString();
                                    sameID.Add(idBarang1);

                                }
                            }
                            areEqual = sameID.GroupBy(x => x).Any(g => g.Count() > 1);
                            if (areEqual)
                            {
                                MessageBox.Show("Tidak boleh memilih barang yang sama!", "Konfirmasi");
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                IDSama = true;
                            }
                            else
                            {
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                IDSama = false;
                            }
                        }
                    }
                }
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (flagAdd)
                {
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    BarangMasukLoad();
                    flagAdd = false;
                }
                if (flagEdit)
                {
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    BarangMasukLoad();
                    flagEdit = false;
                }
            }
            else
            {
                return;
            }
        }
    }
}
