﻿namespace BONIS.Modules
{
    partial class MasterReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControlReport = new System.Windows.Forms.TabControl();
            this.tabRptBarang = new System.Windows.Forms.TabPage();
            this.tabRptStaff = new System.Windows.Forms.TabPage();
            this.tabRptCustomer = new System.Windows.Forms.TabPage();
            this.tabRptSupplier = new System.Windows.Forms.TabPage();
            this.tabRptBarangKeluar = new System.Windows.Forms.TabPage();
            this.tabRptBarangMasuk = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.tabControlReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControlReport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(910, 568);
            this.panel1.TabIndex = 4;
            // 
            // tabControlReport
            // 
            this.tabControlReport.Controls.Add(this.tabRptBarang);
            this.tabControlReport.Controls.Add(this.tabRptStaff);
            this.tabControlReport.Controls.Add(this.tabRptCustomer);
            this.tabControlReport.Controls.Add(this.tabRptSupplier);
            this.tabControlReport.Controls.Add(this.tabRptBarangKeluar);
            this.tabControlReport.Controls.Add(this.tabRptBarangMasuk);
            this.tabControlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlReport.Location = new System.Drawing.Point(10, 10);
            this.tabControlReport.Name = "tabControlReport";
            this.tabControlReport.SelectedIndex = 0;
            this.tabControlReport.Size = new System.Drawing.Size(890, 548);
            this.tabControlReport.TabIndex = 4;
            this.tabControlReport.SelectedIndexChanged += new System.EventHandler(this.tabControlReport_SelectedIndexChanged);
            // 
            // tabRptBarang
            // 
            this.tabRptBarang.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptBarang.Location = new System.Drawing.Point(4, 30);
            this.tabRptBarang.Name = "tabRptBarang";
            this.tabRptBarang.Padding = new System.Windows.Forms.Padding(3);
            this.tabRptBarang.Size = new System.Drawing.Size(882, 514);
            this.tabRptBarang.TabIndex = 0;
            this.tabRptBarang.Text = "Report Barang";
            // 
            // tabRptStaff
            // 
            this.tabRptStaff.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptStaff.Location = new System.Drawing.Point(4, 30);
            this.tabRptStaff.Name = "tabRptStaff";
            this.tabRptStaff.Padding = new System.Windows.Forms.Padding(3);
            this.tabRptStaff.Size = new System.Drawing.Size(882, 514);
            this.tabRptStaff.TabIndex = 1;
            this.tabRptStaff.Text = "Report Staff";
            // 
            // tabRptCustomer
            // 
            this.tabRptCustomer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptCustomer.Location = new System.Drawing.Point(4, 30);
            this.tabRptCustomer.Name = "tabRptCustomer";
            this.tabRptCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabRptCustomer.Size = new System.Drawing.Size(882, 514);
            this.tabRptCustomer.TabIndex = 2;
            this.tabRptCustomer.Text = "Report Customer";
            // 
            // tabRptSupplier
            // 
            this.tabRptSupplier.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptSupplier.Location = new System.Drawing.Point(4, 30);
            this.tabRptSupplier.Name = "tabRptSupplier";
            this.tabRptSupplier.Padding = new System.Windows.Forms.Padding(3);
            this.tabRptSupplier.Size = new System.Drawing.Size(882, 514);
            this.tabRptSupplier.TabIndex = 3;
            this.tabRptSupplier.Text = "Report Supplier";
            // 
            // tabRptBarangKeluar
            // 
            this.tabRptBarangKeluar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptBarangKeluar.Location = new System.Drawing.Point(4, 30);
            this.tabRptBarangKeluar.Name = "tabRptBarangKeluar";
            this.tabRptBarangKeluar.Padding = new System.Windows.Forms.Padding(3);
            this.tabRptBarangKeluar.Size = new System.Drawing.Size(882, 514);
            this.tabRptBarangKeluar.TabIndex = 4;
            this.tabRptBarangKeluar.Text = "Report Barang Keluar";
            // 
            // tabRptBarangMasuk
            // 
            this.tabRptBarangMasuk.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabRptBarangMasuk.Location = new System.Drawing.Point(4, 30);
            this.tabRptBarangMasuk.Name = "tabRptBarangMasuk";
            this.tabRptBarangMasuk.Size = new System.Drawing.Size(882, 514);
            this.tabRptBarangMasuk.TabIndex = 5;
            this.tabRptBarangMasuk.Text = "Report Barang Masuk";
            // 
            // MasterReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MasterReport";
            this.Size = new System.Drawing.Size(910, 568);
            this.Load += new System.EventHandler(this.MasterReport_Load);
            this.panel1.ResumeLayout(false);
            this.tabControlReport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControlReport;
        private System.Windows.Forms.TabPage tabRptBarang;
        private System.Windows.Forms.TabPage tabRptStaff;
        private System.Windows.Forms.TabPage tabRptCustomer;
        private System.Windows.Forms.TabPage tabRptSupplier;
        private System.Windows.Forms.TabPage tabRptBarangKeluar;
        private System.Windows.Forms.TabPage tabRptBarangMasuk;
    }
}
