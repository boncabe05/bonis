﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BONIS.Lib;

namespace BONIS.Modules
{
    public partial class MasterControl : UserControl
    {

        public MasterControl()
        {
            InitializeComponent();
        }

        private  void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    Modules.ControlStaff staff = new Modules.ControlStaff();
                    staff.Dock = DockStyle.Fill;
                    tabStaff.Controls.Clear();
                    tabStaff.Controls.Add(staff);
                    break;
                case 1:
                    Modules.ControlUser user = new Modules.ControlUser();
                    user.Dock = DockStyle.Fill;
                    tabUser.Controls.Clear();
                    tabUser.Controls.Add(user);
                    break;
                case 2:
                    Modules.ControlBarang barang = new Modules.ControlBarang();
                    barang.Dock = DockStyle.Fill;
                    tabBarang.Controls.Clear();
                    tabBarang.Controls.Add(barang);
                    break;
                case 3:
                    Modules.ControlSatuanBarang satuan = new Modules.ControlSatuanBarang();
                    satuan.Dock = DockStyle.Fill;
                    tabSatuan.Controls.Clear();
                    tabSatuan.Controls.Add(satuan);
                    break;
                case 4:
                    Modules.ControlCustomer customer = new Modules.ControlCustomer();
                    customer.Dock = DockStyle.Fill;
                    tabCustomer.Controls.Clear();
                    tabCustomer.Controls.Add(customer);
                    break;
                case 5:
                    Modules.ControlSupplier supplier = new Modules.ControlSupplier();
                    supplier.Dock = DockStyle.Fill;
                    tabSupplier.Controls.Clear();
                    tabSupplier.Controls.Add(supplier);
                    break;
                default:
                    break;
            }
        }

        private void MasterControl_Load(object sender, EventArgs e)
        {
            Modules.ControlStaff staff = new Modules.ControlStaff();
            staff.Dock = DockStyle.Fill;
            tabStaff.Controls.Clear();
            tabStaff.Controls.Add(staff);
        }
    }
}
