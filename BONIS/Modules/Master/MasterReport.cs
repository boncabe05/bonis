﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Threading;

namespace BONIS.Modules
{
    public partial class MasterReport : UserControl
    {
        public MasterReport()
        {
            InitializeComponent();
        }

        private void tabControlReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    Modules.ControlReportBarang rptBarang = new Modules.ControlReportBarang();
                    rptBarang.Dock = DockStyle.Fill;
                    tabRptBarang.Controls.Clear();
                    tabRptBarang.Controls.Add(rptBarang);
                    break;
                case 1:
                    Modules.ControlReportStaff rptStaff = new Modules.ControlReportStaff();
                    rptStaff.Dock = DockStyle.Fill;
                    tabRptStaff.Controls.Clear();
                    tabRptStaff.Controls.Add(rptStaff);
                    break;
                case 2:

                    Modules.ControlReportCustomer rptCustomer = new Modules.ControlReportCustomer();
                    rptCustomer.Dock = DockStyle.Fill;
                    tabRptCustomer.Controls.Clear();
                    tabRptCustomer.Controls.Add(rptCustomer);
                    break;
                case 3:
                    Modules.ControlReportSupplier rptSupplier = new Modules.ControlReportSupplier();
                    rptSupplier.Dock = DockStyle.Fill;
                    tabRptSupplier.Controls.Clear();
                    tabRptSupplier.Controls.Add(rptSupplier);
                    break;
                case 4:
                    Modules.ControlReportBarangKeluar rptBrgKeluar = new Modules.ControlReportBarangKeluar();
                    rptBrgKeluar.Dock = DockStyle.Fill;
                    tabRptBarangKeluar.Controls.Clear();
                    tabRptBarangKeluar.Controls.Add(rptBrgKeluar);
                    break;
                case 5:
                    Modules.ControlReportBarangMasuk rptBrgMasuk = new Modules.ControlReportBarangMasuk();
                    rptBrgMasuk.Dock = DockStyle.Fill;
                    tabRptBarangMasuk.Controls.Clear();
                    tabRptBarangMasuk.Controls.Add(rptBrgMasuk);
                    break;
                default:
                    break;
            }
        }

        private void MasterReport_Load(object sender, EventArgs e)
        {
            Modules.ControlReportBarang rptBarang = new Modules.ControlReportBarang();
            rptBarang.Dock = DockStyle.Fill;
            tabRptBarang.Controls.Clear();
            tabRptBarang.Controls.Add(rptBarang);
        }
    }
}
