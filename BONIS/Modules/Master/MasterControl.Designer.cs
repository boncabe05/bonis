﻿namespace BONIS.Modules
{
    partial class MasterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabStaff = new System.Windows.Forms.TabPage();
            this.tabUser = new System.Windows.Forms.TabPage();
            this.tabBarang = new System.Windows.Forms.TabPage();
            this.tabSatuan = new System.Windows.Forms.TabPage();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.tabSupplier = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(938, 622);
            this.panel1.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabStaff);
            this.tabControl.Controls.Add(this.tabUser);
            this.tabControl.Controls.Add(this.tabBarang);
            this.tabControl.Controls.Add(this.tabSatuan);
            this.tabControl.Controls.Add(this.tabCustomer);
            this.tabControl.Controls.Add(this.tabSupplier);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(10, 10);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(918, 602);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabStaff
            // 
            this.tabStaff.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabStaff.Location = new System.Drawing.Point(4, 30);
            this.tabStaff.Name = "tabStaff";
            this.tabStaff.Padding = new System.Windows.Forms.Padding(3);
            this.tabStaff.Size = new System.Drawing.Size(910, 568);
            this.tabStaff.TabIndex = 0;
            this.tabStaff.Text = "Master Staff";
            // 
            // tabUser
            // 
            this.tabUser.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabUser.Location = new System.Drawing.Point(4, 30);
            this.tabUser.Name = "tabUser";
            this.tabUser.Padding = new System.Windows.Forms.Padding(3);
            this.tabUser.Size = new System.Drawing.Size(910, 568);
            this.tabUser.TabIndex = 1;
            this.tabUser.Text = "Master User";
            // 
            // tabBarang
            // 
            this.tabBarang.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabBarang.Location = new System.Drawing.Point(4, 30);
            this.tabBarang.Name = "tabBarang";
            this.tabBarang.Size = new System.Drawing.Size(910, 568);
            this.tabBarang.TabIndex = 2;
            this.tabBarang.Text = "Master Barang";
            // 
            // tabSatuan
            // 
            this.tabSatuan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabSatuan.Location = new System.Drawing.Point(4, 30);
            this.tabSatuan.Name = "tabSatuan";
            this.tabSatuan.Padding = new System.Windows.Forms.Padding(3);
            this.tabSatuan.Size = new System.Drawing.Size(910, 568);
            this.tabSatuan.TabIndex = 3;
            this.tabSatuan.Text = "Master Satuan Barang";
            // 
            // tabCustomer
            // 
            this.tabCustomer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabCustomer.Location = new System.Drawing.Point(4, 30);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Size = new System.Drawing.Size(910, 568);
            this.tabCustomer.TabIndex = 4;
            this.tabCustomer.Text = "Master Customer";
            // 
            // tabSupplier
            // 
            this.tabSupplier.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabSupplier.Location = new System.Drawing.Point(4, 30);
            this.tabSupplier.Name = "tabSupplier";
            this.tabSupplier.Size = new System.Drawing.Size(910, 568);
            this.tabSupplier.TabIndex = 5;
            this.tabSupplier.Text = "Master Supplier";
            // 
            // MasterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MasterControl";
            this.Size = new System.Drawing.Size(938, 622);
            this.Load += new System.EventHandler(this.MasterControl_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabStaff;
        private System.Windows.Forms.TabPage tabUser;
        private System.Windows.Forms.TabPage tabBarang;
        private System.Windows.Forms.TabPage tabSatuan;
        private System.Windows.Forms.TabPage tabCustomer;
        private System.Windows.Forms.TabPage tabSupplier;
    }
}
