﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace BONIS.Modules
{
    public partial class ControlReportSupplier : UserControl
    {
        private AppData.InventoryEntities db = new AppData.InventoryEntities();
        private string ReportPath = "BONIS.Report.ReportSupplier.rdlc";
        private Lib.ReportControl rpt = new Lib.ReportControl();
        
        private void loadReport(DataTable dt)
        {
            int x = 0;
            ReportParameter[] reportParam = new ReportParameter[x + 1];
            reportParam[0] = new ReportParameter("StaffName", Lib.ManageUser.StaffName);
            ReportDataSource rds = new ReportDataSource("SupplierDataSet", (DataTable)dt);
            this.rptViewer.LocalReport.DataSources.Clear();
            this.rptViewer.LocalReport.DataSources.Add(rds);
            this.rptViewer.LocalReport.ReportEmbeddedResource = ReportPath;
            this.rptViewer.LocalReport.SetParameters(reportParam);
            this.rptViewer.RefreshReport();
        }

        private async void loadAll()
        {
            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {
                DataTable dt = Lib.ReportControl.Supplier.loadSupplier();
                this.loadReport(dt);

            })).ContinueWith(new Action<Task>(task =>
            {
                this.rptViewer.RefreshReport(); ;
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void ControlReportSupplier_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        public ControlReportSupplier()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (radioAlamat.Checked)
            {
                DataTable dt = Lib.ReportControl.Supplier.searchSupplier("alamat", txtSearch.Text);
                loadReport(dt);
            }

            if (radioEmail.Checked)
            {
                DataTable dt = Lib.ReportControl.Supplier.searchSupplier("email", txtSearch.Text);
                loadReport(dt);
            }

            if (radioNamaSupplier.Checked)
            {
                DataTable dt = Lib.ReportControl.Supplier.searchSupplier("nama", txtSearch.Text);
                loadReport(dt);
            }

            if (radioNone.Checked)
            {
                DataTable dt = Lib.ReportControl.Supplier.searchSupplier("none", txtSearch.Text);
                loadReport(dt);
            }

            if (radioPhone.Checked)
            {
                DataTable dt = Lib.ReportControl.Supplier.searchSupplier("phone", txtSearch.Text);
                loadReport(dt);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }
    }
}
