﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Threading;

namespace BONIS.Modules
{
    public partial class ControlReportBarangKeluar : UserControl
    {
        private AppData.InventoryEntities db = new AppData.InventoryEntities();
        private string ReportPath  = "BONIS.Report.ReportBarangKeluar.rdlc";
        private string ParamSearch { get; set; }
        private string ParamPeriode { get; set; }
        private string RptParam { get; set; }
        private string RptPeriode { get; set; }


        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {
                

            })).ContinueWith(new Action<Task>(task =>
            {
                DataTable dt = Lib.ReportControl.BarangKeluar.loadBarangKeluar();
                RptPeriode = "Laporan Perbulan";
                RptParam = "Bulan : " + dateSearch.Value.ToString("MMMM");
                this.loadReport(dt);
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void loadReport(DataTable dt)
        {
            int x = 2;
            ReportParameter[] reportParam = new ReportParameter[x + 1];
            reportParam[0] = new ReportParameter("StaffName", Lib.ManageUser.StaffName);
            reportParam[1] = new ReportParameter("Periode", RptPeriode);
            reportParam[2] = new ReportParameter("DatePeriodes", RptParam);
            ReportDataSource rds = new ReportDataSource("BarangKeluarDataSet", (DataTable)dt);
            this.rptViewer.LocalReport.DataSources.Clear();
            this.rptViewer.LocalReport.DataSources.Add(rds);
            this.rptViewer.LocalReport.ReportEmbeddedResource = ReportPath;
            this.rptViewer.LocalReport.SetParameters(reportParam);
            this.rptViewer.RefreshReport();
        }
        private void ControlReportBarangKeluar_Load(object sender, EventArgs e)
        {
            loadAll();
        }

        public ControlReportBarangKeluar()
        {
            InitializeComponent();   
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DateTime date = dateSearch.Value;
            checkParamRpt();
            if (radioCustomer.Checked)
            {
                ParamSearch = "customer";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioStaff.Checked)
            {
                ParamSearch = "staff";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioJumlah.Checked)
            {
                ParamSearch = "jumlah";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioNone.Checked)
            {
                ParamSearch = "none";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioSatuan.Checked)
            {
                ParamSearch = "satuan";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioBarang.Checked)
            {
                ParamSearch = "barang";
                loadReport(reportPeriode(ParamSearch, txtSearch.Text, date));
            }

            if (radioTanggal.Checked)
            {
                ParamSearch = "tanggal";
                var max = new TimeSpan(23, 59, 59);
                var min = new TimeSpan(0, 0, 0);
                DateTime dateMin_ = dateMin.Value.Date + min;
                DateTime dateMax_ = dateMax.Value.Date + max;
                loadReport(reportDateBetween(dateMin_, dateMax_));
            }
        }

        private void radioTanggal_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = true;
            groupPeriode.Enabled = false;
        }

        private void radioCustomer_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private void radioJumlah_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private void radioBarang_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private void radioStaff_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private void radioSatuan_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private void radioNone_CheckedChanged(object sender, EventArgs e)
        {
            groupTanggal.Enabled = false;
            groupPeriode.Enabled = true;
        }

        private DataTable reportPeriode(string param, string search, DateTime date)
        {
            DataTable dt = new DataTable();
            
            if (groupPeriode.Enabled)
            {
                if (radioPeriodeBulanan.Checked)
                {
                    RptPeriode = "Laporan Perbulan";
                    RptParam = "Bulan : " + dateSearch.Value.ToString("MMMM");
                    ParamPeriode = "bulanan";
                    DateTime bulan = date;
                    dt = Lib.ReportControl.BarangKeluar.searchBarangKeluar(param, search, ParamPeriode, bulan);
                }

                if (radioPeriodeHarian.Checked)
                {
                    RptPeriode = "Laporan Perhari";
                    RptParam = "Tanggal : " + dateSearch.Value.ToString("dd/MM/yyyy");
                    ParamPeriode = "harian";
                    DateTime harian = date;
                    dt = Lib.ReportControl.BarangKeluar.searchBarangKeluar(param, search, ParamPeriode, harian);
                }

                if (radioPeriodeTahunan.Checked)
                {
                    RptPeriode = "Laporan Pertahun";
                    RptParam = "Tahun : " + dateSearch.Value.Year;
                    ParamPeriode = "tahunan";
                    DateTime tahunan = date;
                    dt = Lib.ReportControl.BarangKeluar.searchBarangKeluar(param, search, ParamPeriode, tahunan);
                }
            }
            
            return dt;
        }

        private void checkParamRpt()
        {
            if (groupTanggal.Enabled)
            {
                if (radioTanggal.Checked)
                {
                    RptPeriode = "Laporan Pertanggal";
                    RptParam = "Dari Tanggal : " + dateMin.Value.ToString("dd/MM/yyyy") + " sampai " + dateMax.Value.ToString("dd/MM/yyyy");
                }
            }else 
            if (groupPeriode.Enabled)
            {
                if (radioPeriodeBulanan.Checked)
                {
                    RptPeriode = "Laporan Perbulan";
                    RptParam = "Bulan : " + dateSearch.Value.ToString("MMMM");
                }

                if (radioPeriodeHarian.Checked)
                {
                    RptPeriode = "Laporan Perhari";
                    RptParam = "Tanggal : " + dateSearch.Value.ToString("dd/MM/yyyy");
                }

                if (radioPeriodeTahunan.Checked)
                {
                    RptPeriode = "Laporan Pertahun";
                    RptParam = "Tahun : " + dateSearch.Value.Year;
                }
            }
        }

        private DataTable reportDateBetween(DateTime dateMin_, DateTime dateMax_)
        {
            DataTable dt = new DataTable();
            if (groupTanggal.Enabled)
            {
                dt = Lib.ReportControl.BarangKeluar.searchDateBetween(dateMin_, dateMax_);
            }
            return dt;
        }
    }
}
