﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace BONIS.Modules
{
    public partial class ControlReportCustomer : UserControl
    {
        private AppData.InventoryEntities db = new AppData.InventoryEntities();
        InventoryDataSet inv = new InventoryDataSet();

        private void loadReport(DataTable dt)
        {
            int x = 0;
            ReportParameter[] reportParam = new ReportParameter[x + 1];
            reportParam[0] = new ReportParameter("StaffName", Lib.ManageUser.StaffName);
            ReportDataSource rds = new ReportDataSource("CustomerDataSet", (DataTable)dt);
            this.rptViewer.LocalReport.DataSources.Clear();
            this.rptViewer.LocalReport.DataSources.Add(rds);
            this.rptViewer.LocalReport.ReportEmbeddedResource = "BONIS.Report.ReportCustomer.rdlc";
            this.rptViewer.LocalReport.SetParameters(reportParam);
            this.rptViewer.RefreshReport();
        }

        private async void loadAll()
        {
            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {
                DataTable dt = Lib.ReportControl.Barang.loadBarang();
                this.loadReport(dt);

            })).ContinueWith(new Action<Task>(task =>
            {
                this.rptViewer.RefreshReport(); ;
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void ControlReportCustomer_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        public ControlReportCustomer()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (radioAlamat.Checked)
            {
                DataTable dt = Lib.ReportControl.Customer.searchCustomer("alamat", txtSearch.Text);
                loadReport(dt);
            }

            if (radioEmail.Checked)
            {
                DataTable dt = Lib.ReportControl.Customer.searchCustomer("email", txtSearch.Text);
                loadReport(dt);
            }

            if (radioNamaCustomer.Checked)
            {
                DataTable dt = Lib.ReportControl.Customer.searchCustomer("customer", txtSearch.Text);
                loadReport(dt);
            }

            if (radioNone.Checked)
            {
                DataTable dt = Lib.ReportControl.Customer.searchCustomer("none", txtSearch.Text);
                loadReport(dt);
            }

            if (radioPhone.Checked)
            {
                DataTable dt = Lib.ReportControl.Customer.searchCustomer("phone", txtSearch.Text);
                loadReport(dt);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }
    }
}
