﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace BONIS.Modules
{
    public partial class ControlReportBarang : UserControl
    {
        private AppData.InventoryEntities db = new AppData.InventoryEntities();
        private string ReportPath  = "BONIS.Report.ReportBarang.rdlc";

        private void loadReport(DataTable dt)
        {
            int x = 0;
            ReportParameter[] reportParam = new ReportParameter[x + 1];
            reportParam[0] = new ReportParameter("StaffName", Lib.ManageUser.StaffName);
            ReportDataSource rds = new ReportDataSource("BarangDataSet", (DataTable)dt);
            this.rptViewer.LocalReport.DataSources.Clear();
            this.rptViewer.LocalReport.DataSources.Add(rds);
            this.rptViewer.LocalReport.ReportEmbeddedResource = ReportPath;
            this.rptViewer.LocalReport.SetParameters(reportParam);
            this.rptViewer.RefreshReport(); ;
        }

        private void ControlReportBarang_Load(object sender, EventArgs e)
        {
            loadAll();
        }

        private async void loadAll()
        {
            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {
                DataTable dt = Lib.ReportControl.Barang.loadBarang();
                this.loadReport(dt);

            })).ContinueWith(new Action<Task>(task =>
            {
                this.rptViewer.RefreshReport(); ;
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        public ControlReportBarang()
        {
            InitializeComponent();
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (radioNamaBarang.Checked)
            {
                InventoryDataSet inv = new InventoryDataSet();
                InventoryDataSet.DataBarangDataTable dtBarang = dataBarangTableAdapter.GetDataBarangSatuan();
                DataTable dt = Lib.ReportControl.Barang.searchBarang("barang", txtSearch.Text);
                ReportDataSource rds = new ReportDataSource("BarangDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.RefreshReport();
            }

            if (radioSatuanUnit.Checked)
            {
                InventoryDataSet inv = new InventoryDataSet();
                InventoryDataSet.DataBarangDataTable dtBarang = dataBarangTableAdapter.GetDataBarangSatuan();
                DataTable dt = Lib.ReportControl.Barang.searchBarang("satuan", txtSearch.Text);
                ReportDataSource rds = new ReportDataSource("BarangDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.RefreshReport();
            }

            if (radioStokBarang.Checked)
            {
                InventoryDataSet inv = new InventoryDataSet();
                InventoryDataSet.DataBarangDataTable dtBarang = dataBarangTableAdapter.GetDataBarangSatuan();
                DataTable dt = Lib.ReportControl.Barang.searchBarang("stok", txtSearch.Text);
                ReportDataSource rds = new ReportDataSource("BarangDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.RefreshReport();
            }

            if (radioNone.Checked)
            {
                InventoryDataSet inv = new InventoryDataSet();
                InventoryDataSet.DataBarangDataTable dtBarang = dataBarangTableAdapter.GetDataBarangSatuan();
                DataTable dt = Lib.ReportControl.Barang.searchBarang("none", txtSearch.Text);
                ReportDataSource rds = new ReportDataSource("BarangDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.RefreshReport();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }
    }
}
