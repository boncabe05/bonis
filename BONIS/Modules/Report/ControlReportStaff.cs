﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace BONIS.Modules
{
    public partial class ControlReportStaff : UserControl
    {
        private AppData.InventoryEntities db = new AppData.InventoryEntities();
        private string ReportPath = "BONIS.Report.ReportStaff.rdlc";
        private string ReportPathUsername = "BONIS.Report.ReportStaffUser.rdlc";

        private void loadReport(DataTable dt)
        {
            int x = 0;
            ReportParameter[] reportParam = new ReportParameter[x + 1];
            reportParam[0] = new ReportParameter("StaffName", Lib.ManageUser.StaffName);

            if (checkUsername.Checked)
            {
                ReportDataSource rds = new ReportDataSource("StaffUserDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.LocalReport.ReportEmbeddedResource = ReportPathUsername;
            }
            else
            {
                ReportDataSource rds = new ReportDataSource("StaffDataSet", (DataTable)dt);
                this.rptViewer.LocalReport.DataSources.Clear();
                this.rptViewer.LocalReport.DataSources.Add(rds);
                this.rptViewer.LocalReport.ReportEmbeddedResource = ReportPath;
            }
            this.rptViewer.LocalReport.SetParameters(reportParam);
            this.rptViewer.RefreshReport();
        }

        private async void loadAll()
        {
            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {
                DataTable dt = Lib.ReportControl.Staff.loadStaff(checkUsername.Checked);
                this.loadReport(dt);

            })).ContinueWith(new Action<Task>(task =>
            {
                this.rptViewer.RefreshReport(); ;
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void ControlReportStaff_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        public ControlReportStaff()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (radioEmail.Checked)
            {
                DataTable dt = Lib.ReportControl.Staff.searchStaff("email", txtSearch.Text, checkUsername.Checked);
                loadReport(dt);
            }

            if (radioNamaStaff.Checked)
            {
                DataTable dt = Lib.ReportControl.Staff.searchStaff("nama", txtSearch.Text, checkUsername.Checked);
                loadReport(dt);
            }

            if (radioNone.Checked)
            {
                DataTable dt = Lib.ReportControl.Staff.searchStaff("none", txtSearch.Text, checkUsername.Checked);
                loadReport(dt);
            }

            if (radioPhone.Checked)
            {
                DataTable dt = Lib.ReportControl.Staff.searchStaff("phone", txtSearch.Text, checkUsername.Checked);
                loadReport(dt);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }
    }
}
