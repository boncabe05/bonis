﻿namespace BONIS.Modules
{
    partial class ControlReportBarangKeluar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlReportBarangKeluar));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource7 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupPeriode = new System.Windows.Forms.GroupBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.dateSearch = new System.Windows.Forms.DateTimePicker();
            this.radioPeriodeTahunan = new System.Windows.Forms.RadioButton();
            this.radioPeriodeBulanan = new System.Windows.Forms.RadioButton();
            this.radioPeriodeHarian = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radioNone = new System.Windows.Forms.RadioButton();
            this.radioTanggal = new System.Windows.Forms.RadioButton();
            this.radioSatuan = new System.Windows.Forms.RadioButton();
            this.radioBarang = new System.Windows.Forms.RadioButton();
            this.radioStaff = new System.Windows.Forms.RadioButton();
            this.radioJumlah = new System.Windows.Forms.RadioButton();
            this.radioCustomer = new System.Windows.Forms.RadioButton();
            this.groupTanggal = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateMax = new System.Windows.Forms.DateTimePicker();
            this.dateMin = new System.Windows.Forms.DateTimePicker();
            this.panelReport = new System.Windows.Forms.Panel();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.inventoryDataSet = new BONIS.InventoryDataSet();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupPeriode.SuspendLayout();
            this.groupTanggal.SuspendLayout();
            this.panelReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(910, 568);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.panelReport);
            this.panel3.Location = new System.Drawing.Point(14, 80);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(883, 485);
            this.panel3.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupPeriode);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.txtSearch);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.radioNone);
            this.groupBox1.Controls.Add(this.radioTanggal);
            this.groupBox1.Controls.Add(this.radioSatuan);
            this.groupBox1.Controls.Add(this.radioBarang);
            this.groupBox1.Controls.Add(this.radioStaff);
            this.groupBox1.Controls.Add(this.radioJumlah);
            this.groupBox1.Controls.Add(this.radioCustomer);
            this.groupBox1.Controls.Add(this.groupTanggal);
            this.groupBox1.Location = new System.Drawing.Point(10, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(860, 134);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter By :";
            // 
            // groupPeriode
            // 
            this.groupPeriode.Controls.Add(this.lblSearch);
            this.groupPeriode.Controls.Add(this.dateSearch);
            this.groupPeriode.Controls.Add(this.radioPeriodeTahunan);
            this.groupPeriode.Controls.Add(this.radioPeriodeBulanan);
            this.groupPeriode.Controls.Add(this.radioPeriodeHarian);
            this.groupPeriode.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPeriode.Location = new System.Drawing.Point(237, 14);
            this.groupPeriode.Name = "groupPeriode";
            this.groupPeriode.Size = new System.Drawing.Size(217, 111);
            this.groupPeriode.TabIndex = 13;
            this.groupPeriode.TabStop = false;
            this.groupPeriode.Text = "Periode";
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(13, 81);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(60, 17);
            this.lblSearch.TabIndex = 16;
            this.lblSearch.Text = "Tanggal";
            // 
            // dateSearch
            // 
            this.dateSearch.CustomFormat = "dd-MM-yyyy";
            this.dateSearch.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateSearch.Location = new System.Drawing.Point(90, 81);
            this.dateSearch.Name = "dateSearch";
            this.dateSearch.Size = new System.Drawing.Size(107, 22);
            this.dateSearch.TabIndex = 15;
            // 
            // radioPeriodeTahunan
            // 
            this.radioPeriodeTahunan.AutoSize = true;
            this.radioPeriodeTahunan.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioPeriodeTahunan.Location = new System.Drawing.Point(116, 20);
            this.radioPeriodeTahunan.Name = "radioPeriodeTahunan";
            this.radioPeriodeTahunan.Size = new System.Drawing.Size(81, 21);
            this.radioPeriodeTahunan.TabIndex = 5;
            this.radioPeriodeTahunan.Text = "Tahunan";
            this.radioPeriodeTahunan.UseVisualStyleBackColor = true;
            // 
            // radioPeriodeBulanan
            // 
            this.radioPeriodeBulanan.AutoSize = true;
            this.radioPeriodeBulanan.Checked = true;
            this.radioPeriodeBulanan.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioPeriodeBulanan.Location = new System.Drawing.Point(16, 47);
            this.radioPeriodeBulanan.Name = "radioPeriodeBulanan";
            this.radioPeriodeBulanan.Size = new System.Drawing.Size(78, 21);
            this.radioPeriodeBulanan.TabIndex = 6;
            this.radioPeriodeBulanan.TabStop = true;
            this.radioPeriodeBulanan.Text = "Bulanan";
            this.radioPeriodeBulanan.UseVisualStyleBackColor = true;
            // 
            // radioPeriodeHarian
            // 
            this.radioPeriodeHarian.AutoSize = true;
            this.radioPeriodeHarian.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioPeriodeHarian.Location = new System.Drawing.Point(16, 20);
            this.radioPeriodeHarian.Name = "radioPeriodeHarian";
            this.radioPeriodeHarian.Size = new System.Drawing.Size(68, 21);
            this.radioPeriodeHarian.TabIndex = 8;
            this.radioPeriodeHarian.Text = "Harian";
            this.radioPeriodeHarian.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSearch.BackgroundImage")));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Location = new System.Drawing.Point(764, 100);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 26);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(541, 103);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(217, 23);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(467, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Search :";
            // 
            // radioNone
            // 
            this.radioNone.AutoSize = true;
            this.radioNone.Checked = true;
            this.radioNone.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioNone.Location = new System.Drawing.Point(101, 80);
            this.radioNone.Name = "radioNone";
            this.radioNone.Size = new System.Drawing.Size(61, 21);
            this.radioNone.TabIndex = 4;
            this.radioNone.TabStop = true;
            this.radioNone.Text = "None";
            this.radioNone.UseVisualStyleBackColor = true;
            this.radioNone.CheckedChanged += new System.EventHandler(this.radioNone_CheckedChanged);
            // 
            // radioTanggal
            // 
            this.radioTanggal.AutoSize = true;
            this.radioTanggal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTanggal.Location = new System.Drawing.Point(101, 53);
            this.radioTanggal.Name = "radioTanggal";
            this.radioTanggal.Size = new System.Drawing.Size(78, 21);
            this.radioTanggal.TabIndex = 3;
            this.radioTanggal.Text = "Tanggal";
            this.radioTanggal.UseVisualStyleBackColor = true;
            this.radioTanggal.CheckedChanged += new System.EventHandler(this.radioTanggal_CheckedChanged);
            // 
            // radioSatuan
            // 
            this.radioSatuan.AutoSize = true;
            this.radioSatuan.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSatuan.Location = new System.Drawing.Point(101, 26);
            this.radioSatuan.Name = "radioSatuan";
            this.radioSatuan.Size = new System.Drawing.Size(71, 21);
            this.radioSatuan.TabIndex = 3;
            this.radioSatuan.Text = "Satuan";
            this.radioSatuan.UseVisualStyleBackColor = true;
            this.radioSatuan.CheckedChanged += new System.EventHandler(this.radioSatuan_CheckedChanged);
            // 
            // radioBarang
            // 
            this.radioBarang.AutoSize = true;
            this.radioBarang.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBarang.Location = new System.Drawing.Point(6, 80);
            this.radioBarang.Name = "radioBarang";
            this.radioBarang.Size = new System.Drawing.Size(72, 21);
            this.radioBarang.TabIndex = 3;
            this.radioBarang.Text = "Barang";
            this.radioBarang.UseVisualStyleBackColor = true;
            this.radioBarang.CheckedChanged += new System.EventHandler(this.radioBarang_CheckedChanged);
            // 
            // radioStaff
            // 
            this.radioStaff.AutoSize = true;
            this.radioStaff.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioStaff.Location = new System.Drawing.Point(6, 107);
            this.radioStaff.Name = "radioStaff";
            this.radioStaff.Size = new System.Drawing.Size(54, 21);
            this.radioStaff.TabIndex = 3;
            this.radioStaff.Text = "Staff";
            this.radioStaff.UseVisualStyleBackColor = true;
            this.radioStaff.CheckedChanged += new System.EventHandler(this.radioStaff_CheckedChanged);
            // 
            // radioJumlah
            // 
            this.radioJumlah.AutoSize = true;
            this.radioJumlah.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioJumlah.Location = new System.Drawing.Point(6, 53);
            this.radioJumlah.Name = "radioJumlah";
            this.radioJumlah.Size = new System.Drawing.Size(73, 21);
            this.radioJumlah.TabIndex = 2;
            this.radioJumlah.Text = "Jumlah";
            this.radioJumlah.UseVisualStyleBackColor = true;
            this.radioJumlah.CheckedChanged += new System.EventHandler(this.radioJumlah_CheckedChanged);
            // 
            // radioCustomer
            // 
            this.radioCustomer.AutoSize = true;
            this.radioCustomer.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioCustomer.Location = new System.Drawing.Point(6, 26);
            this.radioCustomer.Name = "radioCustomer";
            this.radioCustomer.Size = new System.Drawing.Size(89, 21);
            this.radioCustomer.TabIndex = 1;
            this.radioCustomer.Text = "Customer";
            this.radioCustomer.UseVisualStyleBackColor = true;
            this.radioCustomer.CheckedChanged += new System.EventHandler(this.radioCustomer_CheckedChanged);
            // 
            // groupTanggal
            // 
            this.groupTanggal.Controls.Add(this.label4);
            this.groupTanggal.Controls.Add(this.label3);
            this.groupTanggal.Controls.Add(this.dateMax);
            this.groupTanggal.Controls.Add(this.dateMin);
            this.groupTanggal.Enabled = false;
            this.groupTanggal.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTanggal.Location = new System.Drawing.Point(478, 17);
            this.groupTanggal.Name = "groupTanggal";
            this.groupTanggal.Size = new System.Drawing.Size(372, 65);
            this.groupTanggal.TabIndex = 14;
            this.groupTanggal.TabStop = false;
            this.groupTanggal.Text = "Tanggal";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(172, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 19);
            this.label4.TabIndex = 14;
            this.label4.Text = "Sampai";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Mulai";
            // 
            // dateMax
            // 
            this.dateMax.CustomFormat = "dd-MM-yyyy ";
            this.dateMax.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateMax.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMax.Location = new System.Drawing.Point(240, 29);
            this.dateMax.Name = "dateMax";
            this.dateMax.Size = new System.Drawing.Size(107, 22);
            this.dateMax.TabIndex = 13;
            // 
            // dateMin
            // 
            this.dateMin.CustomFormat = "dd-MM-yyyy";
            this.dateMin.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateMin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateMin.Location = new System.Drawing.Point(59, 29);
            this.dateMin.Name = "dateMin";
            this.dateMin.Size = new System.Drawing.Size(107, 22);
            this.dateMin.TabIndex = 12;
            // 
            // panelReport
            // 
            this.panelReport.Controls.Add(this.rptViewer);
            this.panelReport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelReport.Location = new System.Drawing.Point(10, 153);
            this.panelReport.Name = "panelReport";
            this.panelReport.Size = new System.Drawing.Size(863, 322);
            this.panelReport.TabIndex = 6;
            // 
            // rptViewer
            // 
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource7.Name = "BarangDataSet";
            reportDataSource7.Value = null;
            this.rptViewer.LocalReport.DataSources.Add(reportDataSource7);
            this.rptViewer.LocalReport.ReportEmbeddedResource = "BONIS.Report.ReportBarang.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(0, 0);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.Size = new System.Drawing.Size(863, 322);
            this.rptViewer.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Location = new System.Drawing.Point(13, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(912, 2);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Report Barang Keluar";
            // 
            // inventoryDataSet
            // 
            this.inventoryDataSet.DataSetName = "InventoryDataSet";
            this.inventoryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ControlReportBarangKeluar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ControlReportBarangKeluar";
            this.Size = new System.Drawing.Size(910, 568);
            this.Load += new System.EventHandler(this.ControlReportBarangKeluar_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupPeriode.ResumeLayout(false);
            this.groupPeriode.PerformLayout();
            this.groupTanggal.ResumeLayout(false);
            this.groupTanggal.PerformLayout();
            this.panelReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioStaff;
        private System.Windows.Forms.RadioButton radioJumlah;
        private System.Windows.Forms.RadioButton radioCustomer;
        private System.Windows.Forms.Panel panelReport;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private InventoryDataSet inventoryDataSet;
        private System.Windows.Forms.RadioButton radioNone;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioTanggal;
        private System.Windows.Forms.RadioButton radioBarang;
        private System.Windows.Forms.RadioButton radioSatuan;
        private System.Windows.Forms.GroupBox groupPeriode;
        private System.Windows.Forms.RadioButton radioPeriodeTahunan;
        private System.Windows.Forms.RadioButton radioPeriodeBulanan;
        private System.Windows.Forms.RadioButton radioPeriodeHarian;
        private System.Windows.Forms.GroupBox groupTanggal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateMax;
        private System.Windows.Forms.DateTimePicker dateMin;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.DateTimePicker dateSearch;
    }
}
