﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlCustomer : UserControl
    {
        bool flagAdd, flagEdit, flagDelete = false;

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Customer");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Customer");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Customer");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Customer");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Customer");
        }
        public ControlCustomer()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvCustomer.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvCustomer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                loadTooltip();
                dgvCustomer.DataSource = Lib.CustomerControl.loadCustomer();
                dgvCustomer.Columns[0].Visible = false;
                this.dgvCustomer.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvCustomer.Refresh();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void customerLoad()
        {
            this.loadAll();
        }

        private void customerSearch(string search)
        {
            dgvCustomer.DataSource = Lib.CustomerControl.searchCustomer(search);
            dgvCustomer.Columns[0].Visible = false;
            this.dgvCustomer.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvCustomer.Refresh();
        }

        private void enabled()
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            txtNamaCustomer.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtAlamat.Enabled = true;
        }

        private void disabled()
        {
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            txtNamaCustomer.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtAlamat.Enabled = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            customerSearch(txtSearch.Text);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string customerName_ = txtNamaCustomer.Text;
            string customerPhone_ = txtPhone.Text;
            string customerEmail_ = txtEmail.Text;
            string customerId_ = txtCustomerID.Text;
            string customerAlamat_ = txtAlamat.Text;
            if (customerName_ != "")
            {
                AppData.dbo_customer customer = new AppData.dbo_customer();
                if (flagAdd)
                {
                    customer.nama_customer = customerName_;
                    customer.phone = customerPhone_;
                    customer.email = customerEmail_;
                    customer.alamat = customerAlamat_;
                    if (Lib.CustomerControl.CRUD(0, customer))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil menambahkan customer";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal menambahkan customer";
                    }
                    customerLoad();
                    clear();
                    disabled();

                }
                if (flagEdit)
                {
                    if (customerId_ != "")
                    {
                        customer.nama_customer = customerName_;
                        customer.phone = customerPhone_;
                        customer.email = customerEmail_;
                        customer.alamat = customerAlamat_;
                        customer.id_customer = Convert.ToInt32(customerId_);
                        if (Lib.CustomerControl.CRUD(1, customer))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil mengedit customer";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal mengedit customer";
                        }
                        customerLoad();
                        clear();
                        disabled();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih customer terlebih dahulu", "Konfirmasi");
                        dgvCustomer.Focus();
                        return;
                    }
                }
                if (flagDelete)
                {
                    if (customerId_ != "")
                    {
                        customer.nama_customer = customerName_;
                        customer.phone = customerPhone_;
                        customer.email = customerEmail_;
                        customer.alamat = customerAlamat_;
                        customer.id_customer = Convert.ToInt32(customerId_);
                        if (Lib.CustomerControl.CRUD(2, customer))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil menghapus customer";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal menghapus customer";
                        }
                        customerLoad();
                        clear();
                        disabled();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih customer terlebih dahulu", "Konfirmasi");
                        dgvCustomer.Focus();
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Nama Customer tidak boleh kosong", "Konfirmasi");
                txtNamaCustomer.Focus();
                return;
            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            clear();
            customerLoad();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtCustomerID.Text != "")
            {
                flagDelete = true;
                enabled();
            }
            else
            {
                MessageBox.Show("Harap pilih customer terlebih dahulu!", "Konfirmasi");
                dgvCustomer.Focus();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void ControlCustomer_Load(object sender, EventArgs e)
        {
            clear();
            customerLoad();
            disabled();
        }

        private void dgvCustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtCustomerID.Text = dgvCustomer[0, e.RowIndex].Value.ToString();
                txtNamaCustomer.Text = dgvCustomer[1, e.RowIndex].Value.ToString();
                txtPhone.Text = dgvCustomer[2, e.RowIndex].Value.ToString();
                txtEmail.Text = dgvCustomer[3, e.RowIndex].Value.ToString();
                txtAlamat.Text = dgvCustomer[4, e.RowIndex].Value.ToString();

            }
        }

        private void clear()
        {
            flagAdd = false; flagEdit = false; flagDelete = false;
            txtNamaCustomer.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtSearch.Text = "";
            txtCustomerID.Text = "";
            txtAlamat.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                clear();
                disabled();
                customerLoad();
                
            }
            else
            {
                return;
            }
        }
    }
}
