﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class MenuAdmin : UserControl
    {
        public MenuAdmin()
        {
            InitializeComponent();
        }

        private void resetButtonColor()
        {
            btnHome.BackColor = Color.WhiteSmoke;
            btnHome.ForeColor = Color.DimGray;
            btnMaster.BackColor = Color.WhiteSmoke;
            btnMaster.ForeColor = Color.DimGray;
            btnBarangKeluar.BackColor = Color.WhiteSmoke;
            btnBarangKeluar.ForeColor = Color.DimGray;
            btnBarangMasuk.BackColor = Color.WhiteSmoke;
            btnBarangMasuk.ForeColor = Color.DimGray;
            btnMaster.BackColor = Color.WhiteSmoke;
            btnMaster.ForeColor = Color.DimGray;
            btnReport.BackColor = Color.WhiteSmoke;
            btnReport.ForeColor = Color.DimGray;
        }

        private void selectedButtonColor(Control controls)
        {
            controls.BackColor = Color.FromArgb(74, 81, 89);
            controls.ForeColor = Color.FromArgb(232, 227, 216);
            HoverPanel.Height = controls.Height;
            HoverPanel.Top = controls.Top;
        }

        public void btnHome_Click(object sender, EventArgs e)
        {
            
        }
        
        private void MenuAdmin_Load(object sender, EventArgs e)
        {

        }

        private void btnRole_Click(object sender, EventArgs e)
        {

        }

        private void btnBarangKeluar_Click(object sender, EventArgs e)
        {
            Modules.ControlBarangKeluar barangKeluar = new Modules.ControlBarangKeluar();
            barangKeluar.Dock = DockStyle.Fill;
            Panel ContentPanel = (this.Parent.Parent.Parent as MainForm).ContentPanel as Panel;
            ContentPanel.Controls.Clear();
            ContentPanel.Controls.Add(barangKeluar);
            btnBarangKeluar.BackColor = Color.FromArgb(0, 122, 204);
            resetButtonColor();
            selectedButtonColor(btnBarangKeluar);
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {

        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            Modules.MasterReport report = new Modules.MasterReport();
            report.Dock = DockStyle.Fill;
            Panel ContentPanel = (this.Parent.Parent.Parent as MainForm).ContentPanel as Panel;
            ContentPanel.Controls.Clear();
            ContentPanel.Controls.Add(report);
            btnReport.BackColor = Color.FromArgb(0, 122, 204);
            resetButtonColor();
            selectedButtonColor(btnReport);
        }

        private void btnHome_MouseHover(object sender, EventArgs e)
        {

        }

        private void btnMaster_Click(object sender, EventArgs e)
        {
            Modules.MasterControl masterData = new Modules.MasterControl();
            masterData.Dock = DockStyle.Fill;
            Panel ContentPanel = (this.Parent.Parent.Parent as MainForm).ContentPanel as Panel;
            ContentPanel.Controls.Clear();
            ContentPanel.Controls.Add(masterData);
            btnMaster.BackColor = Color.FromArgb(0, 122, 204);
            resetButtonColor();
            selectedButtonColor(btnMaster);
        }

        private void btnBarangMasuk_Click(object sender, EventArgs e)
        {
            Modules.ControlBarangMasuk barangMasuk = new Modules.ControlBarangMasuk();
            barangMasuk.Dock = DockStyle.Fill;
            Panel ContentPanel = (this.Parent.Parent.Parent as MainForm).ContentPanel as Panel;
            ContentPanel.Controls.Clear();
            ContentPanel.Controls.Add(barangMasuk);
            btnBarangMasuk.BackColor = Color.FromArgb(0, 122, 204);
            resetButtonColor();
            selectedButtonColor(btnBarangMasuk);
        }
    }
}
