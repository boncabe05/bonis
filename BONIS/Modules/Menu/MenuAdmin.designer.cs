﻿namespace BONIS.Modules
{
    partial class MenuAdmin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuAdmin));
            this.HoverPanel = new System.Windows.Forms.Panel();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnBarangKeluar = new System.Windows.Forms.Button();
            this.btnBarangMasuk = new System.Windows.Forms.Button();
            this.btnMaster = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HoverPanel
            // 
            this.HoverPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.HoverPanel.Location = new System.Drawing.Point(2, 27);
            this.HoverPanel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.HoverPanel.Name = "HoverPanel";
            this.HoverPanel.Size = new System.Drawing.Size(10, 64);
            this.HoverPanel.TabIndex = 6;
            // 
            // btnReport
            // 
            this.btnReport.FlatAppearance.BorderSize = 0;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.btnReport.Image = ((System.Drawing.Image)(resources.GetObject("btnReport.Image")));
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.Location = new System.Drawing.Point(20, 324);
            this.btnReport.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnReport.Name = "btnReport";
            this.btnReport.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnReport.Size = new System.Drawing.Size(144, 64);
            this.btnReport.TabIndex = 7;
            this.btnReport.Text = "              Report";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnBarangKeluar
            // 
            this.btnBarangKeluar.FlatAppearance.BorderSize = 0;
            this.btnBarangKeluar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBarangKeluar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBarangKeluar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.btnBarangKeluar.Image = ((System.Drawing.Image)(resources.GetObject("btnBarangKeluar.Image")));
            this.btnBarangKeluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBarangKeluar.Location = new System.Drawing.Point(20, 250);
            this.btnBarangKeluar.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBarangKeluar.Name = "btnBarangKeluar";
            this.btnBarangKeluar.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnBarangKeluar.Size = new System.Drawing.Size(144, 64);
            this.btnBarangKeluar.TabIndex = 7;
            this.btnBarangKeluar.Text = "              Barang Keluar";
            this.btnBarangKeluar.UseVisualStyleBackColor = false;
            this.btnBarangKeluar.Click += new System.EventHandler(this.btnBarangKeluar_Click);
            // 
            // btnBarangMasuk
            // 
            this.btnBarangMasuk.FlatAppearance.BorderSize = 0;
            this.btnBarangMasuk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBarangMasuk.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBarangMasuk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.btnBarangMasuk.Image = ((System.Drawing.Image)(resources.GetObject("btnBarangMasuk.Image")));
            this.btnBarangMasuk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBarangMasuk.Location = new System.Drawing.Point(20, 176);
            this.btnBarangMasuk.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBarangMasuk.Name = "btnBarangMasuk";
            this.btnBarangMasuk.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnBarangMasuk.Size = new System.Drawing.Size(144, 64);
            this.btnBarangMasuk.TabIndex = 7;
            this.btnBarangMasuk.Text = "              Barang Masuk";
            this.btnBarangMasuk.UseVisualStyleBackColor = false;
            this.btnBarangMasuk.Click += new System.EventHandler(this.btnBarangMasuk_Click);
            // 
            // btnMaster
            // 
            this.btnMaster.FlatAppearance.BorderSize = 0;
            this.btnMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaster.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaster.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.btnMaster.Image = ((System.Drawing.Image)(resources.GetObject("btnMaster.Image")));
            this.btnMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaster.Location = new System.Drawing.Point(20, 102);
            this.btnMaster.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnMaster.Name = "btnMaster";
            this.btnMaster.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnMaster.Size = new System.Drawing.Size(144, 64);
            this.btnMaster.TabIndex = 7;
            this.btnMaster.Text = "              Master Data\r\n";
            this.btnMaster.UseVisualStyleBackColor = false;
            this.btnMaster.Click += new System.EventHandler(this.btnMaster_Click);
            // 
            // btnHome
            // 
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(101)))), ((int)(((byte)(147)))));
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(22, 28);
            this.btnHome.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnHome.Name = "btnHome";
            this.btnHome.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnHome.Size = new System.Drawing.Size(144, 64);
            this.btnHome.TabIndex = 7;
            this.btnHome.Text = "              Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            this.btnHome.MouseHover += new System.EventHandler(this.btnHome_MouseHover);
            // 
            // MenuAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.HoverPanel);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.btnBarangKeluar);
            this.Controls.Add(this.btnBarangMasuk);
            this.Controls.Add(this.btnMaster);
            this.Controls.Add(this.btnHome);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximumSize = new System.Drawing.Size(172, 622);
            this.Name = "MenuAdmin";
            this.Size = new System.Drawing.Size(172, 622);
            this.Load += new System.EventHandler(this.MenuAdmin_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel HoverPanel;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnMaster;
        private System.Windows.Forms.Button btnBarangMasuk;
        private System.Windows.Forms.Button btnBarangKeluar;
        private System.Windows.Forms.Button btnReport;
    }
}
