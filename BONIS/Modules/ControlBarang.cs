﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlBarang : UserControl
    {
        private bool flagAdd, flagEdit = false;
        private bool handleKey = false;
        Lib.BarangClass barang;

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Barang");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Barang");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Barang");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Barang");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Barang");
        }
        public ControlBarang()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvBarang.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                loadTooltip();
                clear();
                disabled();
                this.dgvBarang.Columns[0].Visible = false;
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void ControlBarang_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgvBarang.DataSource = Lib.BarangControl.searchBarang(txtSearch.Text);
            dgvBarang.Refresh();
        }

        private void txtStok_KeyPress(object sender, KeyPressEventArgs e)
        {
            handleKey = true;
            for (int h = 58; h <= 127; h++)
            {
                if (e.KeyChar == h)
                {
                    e.Handled = true;
                    handleKey = false;
                }
            }
            for (int k = 32; k <= 47; k++)
            {
                if (e.KeyChar == k) 
                {
                    e.Handled = true;
                    handleKey = false;
                }
            }
        }

        private void clear()
        {
            txtNamaBarang.Text = "";
            txtStok.Text = "";
            txtID.Text = "";
            flagEdit = false;
            flagAdd = false;
            dgvBarang.DataSource = Lib.BarangControl.loadBarang();
            dgvBarang.Refresh();
            Lib.SatuanBarangControl.loadSatuanList(cboSatuan);
        }

        private void enabled()
        {
            txtNamaBarang.Enabled = true;
            txtStok.Enabled = true;
            cboSatuan.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void disabled()
        {
            txtNamaBarang.Enabled = false;
            txtStok.Enabled = false;
            cboSatuan.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        int stok = 0;
        private void txtStok_KeyUp(object sender, KeyEventArgs e)
        {
            if (handleKey)
            {
                if(txtStok.Text == "")
                {
                    stok = 0;
                    txtStok.Text = "0";
                }
                else
                {
                    if (txtStok.MaxLength > txtStok.MaxLength)
                    {
                        MessageBox.Show("Stok melebihi batas");
                        return;
                    }
                    else
                    {
                        string temp = txtStok.Text.Replace(",", "");
                        stok = Convert.ToInt32(temp);
                        txtStok.Text = String.Format("{0:#,#}", stok);
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if(txtID.Text == "")
            {
                MessageBox.Show("Harap pilih data yang ingin di edit");
                dgvBarang.Focus();
            }
            else
            {
                enabled();
                flagEdit = true;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            dgvBarang.DataSource = Lib.BarangControl.loadBarang();
            dgvBarang.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            barang = new Lib.BarangClass();
            if (flagAdd)
            {
                int satuan = Convert.ToInt32(cboSatuan.SelectedValue.ToString());
                barang.NamaBarang = txtNamaBarang.Text;
                string temp = txtStok.Text.Replace(",", "");
                barang.StokBarang = Convert.ToInt32(txtStok.Text);
                barang.ID_Satuan = satuan;
                if(Lib.BarangControl.Crud(0, barang))
                {
                    Lib.NotificationMessage.Notification = "Berhasil Menambahkan Barang";
                }
                else
                {
                    Lib.NotificationMessage.Notification = "Gagal Menambahkan Barang";
                }
            }
            else if (flagEdit)
            {
                int satuan = Convert.ToInt32(cboSatuan.SelectedValue.ToString());
                barang.ID = Convert.ToInt32(txtID.Text);
                barang.NamaBarang = txtNamaBarang.Text;
                string temp = txtStok.Text.Replace(",", "");
                barang.StokBarang = Convert.ToInt32(txtStok.Text);
                barang.ID_Satuan = satuan;
                if (Lib.BarangControl.Crud(1, barang))
                {
                    Lib.NotificationMessage.Notification = "Berhasil Mengedit Barang";
                }
                else
                {
                    Lib.NotificationMessage.Notification = "Gagal Mengedit Barang";
                }
            }
            else
            {
                barang.ID = Convert.ToInt32(txtID.Text);
                if (Lib.BarangControl.Crud(2, barang))
                {
                    Lib.NotificationMessage.Notification = "Berhasil Menghapus Barang";
                }
                else
                {
                    Lib.NotificationMessage.Notification = "Gagal Menghapus Barang";
                }
            }
            
            clear();
            disabled();
        }

        private void dgvBarang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtID.Text = dgvBarang[0, e.RowIndex].Value.ToString();
                txtNamaBarang.Text = dgvBarang[1, e.RowIndex].Value.ToString();
                txtStok.Text = dgvBarang[2, e.RowIndex].Value.ToString();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (flagAdd)
                {
                    clear();
                    disabled();
                    flagAdd = false;
                }
                if (flagEdit)
                {
                    clear();
                    disabled();
                    flagEdit = false;
                }
            }
            else
            {
                return;
            }
        }
    }
}
