﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlUser : UserControl
    {

        bool flagEdit, flagChangePassword = false;
        public ControlUser()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvUser.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvUser.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                loadUser();
                clear();
                disabled();
                loadTooltip();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan User");
            toolTipButton.SetToolTip(this.btnSearch, "Cari User");
            toolTipButton.SetToolTip(this.btnEdit, "Edit User");
            toolTipButton.SetToolTip(this.btnCancelPassword, "Cancel edit password");
            toolTipButton.SetToolTip(this.btnUnlockPassword, "Edit Password");
            toolTipButton.SetToolTip(this.btnViewPassword, "Lihat Password");
            toolTipButton.SetToolTip(this.btnCloseViewPassword, "Tutup Password");
        }

        private void enabled()
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnEdit.Enabled = false;
            txtUsername.Enabled = true;
            btnCancelPassword.Enabled = true;
            btnUnlockPassword.Enabled = true;
        }

        private void disabled()
        {
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnEdit.Enabled = true;
            txtUsername.Enabled = false;
            txtPassword.Enabled = false;
            btnCancelPassword.Enabled = false;
            btnUnlockPassword.Enabled = false;
        }

        private void loadUser()
        {
            dgvUser.DataSource = Lib.UsersControl.loadUser();
            dgvUser.Columns[0].Visible = false;
            dgvUser.Refresh();
        }

        private void ControlUser_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgvUser.DataSource = Lib.UsersControl.searchUser(txtSearch.Text);
            dgvUser.Refresh();
        }

        private void clear()
        {
            flagEdit = false;
            txtPassword.Text = "";
            txtSearch.Text = "";
            txtUsername.Text = "";
            txtUserID.Text = "";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            clear();
            loadUser();
            disabled();
        }

        private void btnCancelPassword_Click(object sender, EventArgs e)
        {
            flagChangePassword = false;
            btnViewPassword.Visible = false;
            btnCloseViewPassword.Visible = false;
            txtPassword.UseSystemPasswordChar = true;
            txtPassword.Text = "";
            txtPassword.Enabled = false;
            btnUnlockPassword.Visible = true;
        }

        private void btnUnlockPassword_Click(object sender, EventArgs e)
        {
            flagChangePassword = true;
            txtPassword.Enabled = true;
            btnViewPassword.Visible = true;
            btnUnlockPassword.Visible = false;
        }

        private void btnViewPassword_Click(object sender, EventArgs e)
        {
            btnCloseViewPassword.Visible = true;
            txtPassword.UseSystemPasswordChar = false;
            btnViewPassword.Visible = false;
        }

        private void btnCloseViewPassword_Click(object sender, EventArgs e)
        {
            btnViewPassword.Visible = true;
            txtPassword.UseSystemPasswordChar = true;
            btnCloseViewPassword.Visible = false;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
                txtSearch.Focus();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                clear();
                disabled();
                btnCloseViewPassword.PerformClick();
            }
            else
            {
                return;
            }
        }

        private bool ValidateUsername()
        {
            bool result = false;
            string username_ = txtUsername.Text.ToLower();
            if (flagEdit)
            {
                int userID = Convert.ToInt32(txtUserID.Text);
                if (Lib.UsersControl.checkUsernameEdit(username_, userID))
                {
                    txtUsername.Focus();
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        private void dgvUser_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtUserID.Text = dgvUser[0, e.RowIndex].Value.ToString();
                txtUsername.Text = dgvUser[1, e.RowIndex].Value.ToString();
                txtPassword.Text = "New Password";
                btnUnlockPassword.Visible = true;
                btnCancelPassword.Visible = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string userUsername = txtUsername.Text.ToLower();
            string userPassword = txtPassword.Text.ToLower();
            string userUserID = txtUserID.Text;
            if (userUsername != "")
            {
                AppData.dbo_user user = new AppData.dbo_user();
                if (flagEdit)
                {
                    if (userUserID != "" && userUsername != "")
                    {
                        if (ValidateUsername())
                        {
                            user.username = userUsername;
                            user.password = userPassword;
                            user.user_id = Convert.ToInt32(userUserID);
                            if (Lib.UsersControl.CRUD(1, user, flagChangePassword))
                            {
                                Lib.NotificationMessage.Notification = "Berhasil mengedit user";
                            }
                            else
                            {
                                Lib.NotificationMessage.Notification = "Berhasil mengedit user";
                            }
                            loadUser();
                            clear();
                            disabled();
                        }
                        else
                        {
                            MessageBox.Show("Username sudah ada, harap ganti dengan username lain", "Konfirmasi");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih user terlebih dahulu", "Konfirmasi");
                        dgvUser.Focus();
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Username tidak boleh kosong", "Konfirmasi");
                txtUsername.Focus();
                return;
            }

        }
    }
}
