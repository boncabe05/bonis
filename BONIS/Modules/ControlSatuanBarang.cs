﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlSatuanBarang : UserControl
    {
        bool flagAdd, flagEdit, flagDelete = false;

        public ControlSatuanBarang()
        {
            InitializeComponent();
            
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvSatuan.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                clear();
                disabled();
                loadSatuan();
                loadTooltip();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Satuan");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Satuan");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Satuan");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Satuan");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Satuan");
        }

        private void clear()
        {
            txtSatuan.Text = "";
            txtSearch.Text = "";
            flagAdd = false;
            flagEdit = false;
            flagDelete = false;
        }

        private void enabled()
        {
            txtSatuan.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnRefresh.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        private void disabled()
        {
            txtSatuan.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            btnRefresh.Enabled = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        private void loadSatuan()
        {
            dgvSatuan.DataSource = Lib.SatuanBarangControl.loadSatuan();
            dgvSatuan.Refresh();
            dgvSatuan.Columns[0].Visible = false;
        }

        private void ControlSatuanBarang_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgvSatuan.DataSource = Lib.SatuanBarangControl.searchSatuan(txtSearch.Text);
            dgvSatuan.Columns[0].Visible = false;
            dgvSatuan.Refresh();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            enabled();
            flagDelete = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string namasatuan_ = txtSatuan.Text;
            string idsatuan_ = txtSatuanID.Text;
            if (namasatuan_ != "")
            {
                if (flagAdd)
                {
                    AppData.dbo_satuan satuan = new AppData.dbo_satuan();
                    satuan.nama_satuan = namasatuan_;
                    if (Lib.SatuanBarangControl.Crud(0, satuan))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil tambah satuan";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal tambah satuan";
                    }
                    clear();
                    disabled();
                    loadSatuan();
                }
                if (flagEdit)
                {
                    if (idsatuan_ != "")
                    {
                        AppData.dbo_satuan satuan = new AppData.dbo_satuan();
                        satuan.nama_satuan = namasatuan_;
                        satuan.id_satuan = Convert.ToInt32(idsatuan_);
                        if (Lib.SatuanBarangControl.Crud(1, satuan))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil edit satuan";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal edit satuan";
                        }
                        clear();
                        disabled();
                        loadSatuan();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih satuan", "Konfirmasi");
                        dgvSatuan.Focus();
                        return;
                    }
                }
                if (flagDelete)
                {
                    if (idsatuan_ != "")
                    {
                        AppData.dbo_satuan satuan = new AppData.dbo_satuan();
                        satuan.nama_satuan = namasatuan_;
                        satuan.id_satuan = Convert.ToInt32(idsatuan_);
                        if (Lib.SatuanBarangControl.Crud(2, satuan))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil hapus satuan";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Gagal hapus satuan";
                        }
                        clear();
                        disabled();
                        loadSatuan();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih satuan", "Konfirmasi");
                        dgvSatuan.Focus();
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Nama Satuan tidak boleh kosong", "Konfirmasi");
                txtSatuan.Focus();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                clear();
                disabled();
                flagAdd = false;
                flagEdit = false;
            }
            else
            {
                return;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            clear();
            disabled();
            loadSatuan();
        }
    }
}
