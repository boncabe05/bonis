﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace BONIS.Modules
{
    public partial class ControlBarangKeluar : UserControl
    {
        private bool flagAdd, flagEdit, flagDelete = false;
        bool MelebihiStok = false;
        bool IDSama = false;

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Barang Keluar");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Barang Keluar");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Barang Keluar");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Barang Keluar");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Barang Keluar");
        }
        public ControlBarangKeluar()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvBarangKeluar.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvTemp.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                dgvTemp.EnableHeadersVisualStyles = false;
                clear();
                disabled();
                tempLoad();
                cboLoad();
                barangKeluarLoad();
                loadTooltip();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }


        private void ControlBarangKeluar_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void tempLoad()
        {
            dgvCboNamaBarang.DataSource = Lib.BarangKeluarControl.loadBarang();
            dgvCboNamaBarang.DisplayMember = "nama_barang";
            dgvCboNamaBarang.ValueMember = "id_barang";
            dgvCboJumlah.DataSource = Lib.BarangKeluarControl.loadJumlah();
            dgvCboJumlah.DisplayMember = "idjumlah";
            dgvCboJumlah.ValueMember = "jumlah";

        }

        private void cboLoad()
        {
            cboCustomer.DataSource = Lib.CustomerControl.loadCustomerList();
            cboCustomer.DisplayMember = "nama_customer";
            cboCustomer.ValueMember = "id_customer";
        }

        private void barangKeluarLoad()
        {
            dgvBarangKeluar.DataSource = Lib.BarangKeluarControl.loadBarangKeluar();
            dgvBarangKeluar.Columns[0].Visible = false;
            dgvBarangKeluar.Columns[1].Visible = false;
            dgvBarangKeluar.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvBarangKeluar.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void clear()
        {
            dgvTemp.Rows.Clear();
            txtIDBarangKeluar.Text = "";
            if (dgvTemp.Columns.Contains("cImg"))
            {
                dgvTemp.Columns.Remove("cImg");
            }
        }

        private void enabled()
        {
            cboCustomer.Enabled = true;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            dgvTemp.Enabled = true;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dgvTemp.RowsDefaultCellStyle.BackColor = Color.White;
            dgvTemp.DefaultCellStyle.BackColor = SystemColors.Window;
            dgvTemp.DefaultCellStyle.ForeColor = SystemColors.ControlText;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
            dgvTemp.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ReadOnly = false;
            dgvTemp.EnableHeadersVisualStyles = true;
        }

        private void disabled()
        {
            cboCustomer.Enabled = false;
            dgvTemp.Enabled = false;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = Color.Gray;
            dgvTemp.RowsDefaultCellStyle.BackColor = Color.Gray;
            dgvTemp.DefaultCellStyle.BackColor = SystemColors.GrayText;
            dgvTemp.DefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
            dgvTemp.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
            dgvTemp.ReadOnly = true;
            dgvTemp.EnableHeadersVisualStyles = false;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            flagAdd = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            dgvBarangKeluar.DataSource = Lib.BarangKeluarControl.loadBarangKeluar();
            dgvBarangKeluar.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!MelebihiStok && !IDSama)
            {
                List<Lib.BarangKeluarLItem> items = new List<Lib.BarangKeluarLItem>();
                AppData.dbo_barang_keluar barang_keluar = new AppData.dbo_barang_keluar();
                Label idUser = (this.Parent.Parent.Parent as MainForm).lblIDUser as Label;
                int id_user = Convert.ToInt32(idUser.Text);
                if (flagAdd)
                {
                    barang_keluar.user_id = id_user;
                    barang_keluar.id_customer = Convert.ToInt32(cboCustomer.SelectedValue.ToString());
                    barang_keluar.tanggal = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    foreach (DataGridViewRow item in dgvTemp.Rows)
                    {
                        if (null != item && null != item.Cells[1].Value)
                        {
                            items.Add(new Lib.BarangKeluarLItem
                            {
                                idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                            });
                        }
                    }

                    if (Lib.BarangKeluarControl.Crud(0, barang_keluar, items))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Menambahkan Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Menambahkan Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    barangKeluarLoad();
                    flagAdd = false;
                }
                if (flagEdit)
                {
                    barang_keluar.user_id = id_user;
                    barang_keluar.id_customer = Convert.ToInt32(cboCustomer.SelectedValue.ToString());
                    barang_keluar.tanggal = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    foreach (DataGridViewRow item in dgvTemp.Rows)
                    {
                        if (null != item && null != item.Cells[1].Value)
                        {
                            if (item.Visible == true)
                            {
                                int idKeluarLItem = (item != null && item.Cells[0].Value != null) ? Convert.ToInt32(item.Cells[0].Value.ToString()) : 0;
                                items.Add(new Lib.BarangKeluarLItem
                                {
                                    idBarangKeluarLitem = idKeluarLItem,
                                    idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                    Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                                    idBarangKeluar = Convert.ToInt32(txtIDBarangKeluar.Text),
                                    isDeleted = false
                                });

                            }
                            else
                            {
                                int idKeluarLItem = (item != null && item.Cells[0].Value != null) ? Convert.ToInt32(item.Cells[0].Value.ToString()) : 0;
                                items.Add(new Lib.BarangKeluarLItem
                                {
                                    idBarangKeluarLitem = Convert.ToInt32(item.Cells[0].Value.ToString()),
                                    idBarang = Convert.ToInt32(item.Cells[1].Value.ToString()),
                                    Jumlah = Convert.ToInt32(item.Cells[2].Value.ToString()),
                                    idBarangKeluar = Convert.ToInt32(txtIDBarangKeluar.Text),
                                    isDeleted = true
                                });
                            }
                        }
                    }

                    if (Lib.BarangKeluarControl.Crud(1, barang_keluar, items, txtIDBarangKeluar.Text))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Mengedit Data Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Mengedit Data Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    barangKeluarLoad();
                    flagEdit = false;
                }
                if (flagDelete)
                {
                    if (Lib.BarangKeluarControl.Crud(2, barang_keluar, items, txtIDBarangKeluar.Text))
                    {
                        Lib.NotificationMessage.Notification = "Berhasil Menghapus Data Barang";
                    }
                    else
                    {
                        Lib.NotificationMessage.Notification = "Gagal Menghapus Data Barang";
                    }
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    barangKeluarLoad();
                    flagEdit = false;
                }
            }
            else
            {
                if (MelebihiStok) { MessageBox.Show("Jumlah barang melebihi stok"); return; }
                if (IDSama) { MessageBox.Show("Barang tidak boleh sama"); return; }
            }
        }

        private void dgvBarang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgvBarangKeluar[0, e.RowIndex].Value != null && dgvBarangKeluar[0, e.RowIndex].Value.ToString() != "")
                {
                    txtIDBarangKeluar.Text = dgvBarangKeluar[0, e.RowIndex].Value.ToString();
                    cboCustomer.SelectedValue = dgvBarangKeluar[1, e.RowIndex].Value;
                    int idkeluar = Convert.ToInt32(txtIDBarangKeluar.Text);
                    loadDataLitem(idkeluar);
                }
            }
        }

        private void loadDataLitem(int idKeluar)
        {
            dgvTemp.Rows.Clear();
            var items = Lib.BarangKeluarControl.loadBarangKeluarList(idKeluar);
            int i = 0;
            if (!dgvTemp.Columns.Contains("cImg"))
            {
                DataGridViewImageColumn ic = new DataGridViewImageColumn();
                ic.HeaderText = "Delete";
                ic.Image = null;
                ic.Name = "cImg";
                ic.Width = 100;
                dgvTemp.Columns.Add(ic);
            }
            foreach (var item in items)
            {
                dgvTemp.Rows.Add();
                dgvTemp.Rows[i].Cells[0].Value = item.idBarangKeluarLitem;
                dgvTemp.Rows[i].Cells[1].Value = item.idBarang;
                dgvTemp.Rows[i].Cells[2].Value = item.Jumlah;
                dgvTemp.Rows[i].Cells[3].Value = (System.Drawing.Image)Properties.Resources.icons8_delete_16;
                i++;
            }

        }

        private void dgvTemp_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgvTemp.Columns[2].Index == e.ColumnIndex)
                {
                    try
                    {
                        if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                        {
                            int idbarang = Convert.ToInt32(dgvTemp[1, e.RowIndex].Value.ToString());
                            if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                            {
                                int stok = Convert.ToInt32(dgvTemp[2, e.RowIndex].Value.ToString());
                                bool outOfStok = Lib.BarangKeluarControl.checkStokBarang(idbarang, stok);
                                if (outOfStok)
                                {
                                    MessageBox.Show("Jumlah barang melebihi stok!", "Konfirmasi");
                                    dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                }
                                else
                                {
                                    dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                }
                            }
                        }
                    }
                    catch
                    {
                        
                    }
                }

                if (dgvTemp.Columns[1].Index == e.ColumnIndex)
                {
                    bool areEqual = false;
                    if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                    {
                        List<string> sameID = new List<string>();
                        if (dgvTemp.RowCount > 2)
                        {

                            foreach (DataGridViewRow row in dgvTemp.Rows)
                            {
                                if (null != row && null != row.Cells[1].Value)
                                {
                                    string idBarang1 = row.Cells[1].Value.ToString();
                                    sameID.Add(idBarang1);

                                }
                            }
                            areEqual = sameID.GroupBy(x => x).Any(g => g.Count() > 1);
                            if (areEqual)
                            {
                                MessageBox.Show("Tidak boleh memilih barang yang sama!", "Konfirmasi");
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                IDSama = true;
                            }
                            else
                            {
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                IDSama = false;
                            }
                        }
                    }
                }
            }
        }

        private void dgvTemp_RowLeave(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvTemp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTemp.Columns.Contains("cImg"))
            {
                if (dgvTemp.Columns[3].Index == e.ColumnIndex)
                {
                    var senderGrid = (DataGridView)sender;
                    var column = senderGrid.Columns[3];
                    if (e.RowIndex >= 0)
                    {
                        dgvTemp.Rows[e.RowIndex].Visible = false;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            enabled();
            flagDelete = true;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
                txtSearch.Focus();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string search = txtSearch.Text;
            dgvBarangKeluar.DataSource = Lib.BarangKeluarControl.searchBarangKeluar(search);
            dgvBarangKeluar.Columns[0].Visible = false;
            dgvBarangKeluar.Columns[1].Visible = false;
            dgvBarangKeluar.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvBarangKeluar.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void dgvTemp_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgvTemp.Columns[2].Index == e.ColumnIndex)
                {
                    try
                    {
                        if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                        {
                            int idbarang = Convert.ToInt32(dgvTemp[1, e.RowIndex].Value.ToString());
                            if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                            {
                                int stok = Convert.ToInt32(dgvTemp[2, e.RowIndex].Value.ToString());
                                bool outOfStok = Lib.BarangKeluarControl.checkStokBarang(idbarang, stok);
                                if (outOfStok)
                                {
                                    MessageBox.Show("Jumlah barang melebihi stok!", "Konfirmasi");
                                    dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                }
                                else
                                {
                                    dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                if (dgvTemp.Columns[1].Index == e.ColumnIndex)
                {
                    bool areEqual = false;
                    if (dgvTemp[1, e.RowIndex].Value != null || dgvTemp[1, e.RowIndex].Value.ToString() != "")
                    {
                        List<string> sameID = new List<string>();
                        if (dgvTemp.RowCount > 2)
                        {
                            foreach (DataGridViewRow row in dgvTemp.Rows)
                            {
                                if (null != row && null != row.Cells[1].Value)
                                {
                                    string idBarang1 = row.Cells[1].Value.ToString();
                                    sameID.Add(idBarang1);
                                }
                            }
                            areEqual = sameID.GroupBy(x => x).Any(g => g.Count() > 1);
                            if (areEqual)
                            {
                                MessageBox.Show("Tidak boleh memilih barang yang sama!", "Konfirmasi");
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                                IDSama = true;
                            }
                            else
                            {
                                dgvTemp.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                                IDSama = false;
                            }
                        }
                    }
                }

            }


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (flagAdd)
                {
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    barangKeluarLoad();
                    flagAdd = false;
                }
                if (flagEdit)
                {
                    clear();
                    disabled();
                    tempLoad();
                    cboLoad();
                    barangKeluarLoad();
                    flagEdit = false;
                }
            }
            else
            {
                return;
            }
        }
    }
}
