﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BONIS.Modules
{
    public partial class ControlStaff : UserControl
    {
        bool flagAdd, flagEdit, flagDelete, flagChangePassword = false;

        public ControlStaff()
        {
            InitializeComponent();
        }

        private async void loadAll()
        {

            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            await Task.Run(new Action(() =>
            {

            })).ContinueWith(new Action<Task>(task =>
            {
                this.dgvStaff.DefaultCellStyle.Font = new Font("Century Gothic", 10);
                this.dgvStaff.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                clear();
                staffLoad();
                disabled();
                loadTooltip();
            }), TaskScheduler.FromCurrentSynchronizationContext());
            load_ = (Loading)Application.OpenForms["Loading"];
            load_.Close();
        }

        private void loadTooltip()
        {
            toolTipButton.SetToolTip(this.btnAdd, "Tambah Staff");
            toolTipButton.SetToolTip(this.btnCancel, "Cancel");
            toolTipButton.SetToolTip(this.btnDelete, "Hapus Staff");
            toolTipButton.SetToolTip(this.btnRefresh, "Refresh Data");
            toolTipButton.SetToolTip(this.btnSave, "Simpan Staff");
            toolTipButton.SetToolTip(this.btnSearch, "Cari Staff");
            toolTipButton.SetToolTip(this.btnEdit, "Edit Staff");
            toolTipButton.SetToolTip(this.btnCancelPassword, "Cancel edit password");
            toolTipButton.SetToolTip(this.btnUnlockPassword, "Edit Password");
            toolTipButton.SetToolTip(this.btnViewPassword, "Lihat Password");
            toolTipButton.SetToolTip(this.btnCloseViewPassword, "Tutup Password");
        }

        private void ControlSatuanBarang_Load(object sender, EventArgs e)
        {
            this.loadAll();
        }

        private void staffLoad()
        {
            dgvStaff.DataSource = Lib.StaffControl.loadStaff();
            dgvStaff.Columns[0].Visible = false;
            dgvStaff.Columns[4].Visible = false;
            dgvStaff.Refresh();
        }

        private void staffSearch(string search)
        {
            dgvStaff.DataSource = Lib.StaffControl.searchStaff(search);
            dgvStaff.Columns[0].Visible = false;
            dgvStaff.Columns[4].Visible = false;
            dgvStaff.Refresh();
        }

        private void enabled()
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            txtNamaStaff.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtUsername.Enabled = true;
        }

        private void disabled()
        {
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            txtNamaStaff.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtUsername.Enabled = false;
            txtPassword.Enabled = false;
            btnCancelPassword.Visible = false;
            btnUnlockPassword.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            staffSearch(txtSearch.Text);
        }

        private bool ValidateUsername()
        {
            bool result = false;
            string username_ = txtUsername.Text.ToLower();
            if (flagEdit)
            {
                int userID = Convert.ToInt32(txtUserID.Text);
                if (Lib.StaffControl.checkUsernameEdit(username_, userID))
                {
                    txtUsername.Focus();
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            else
            {
                if (Lib.StaffControl.checkUsername(username_))
                {
                    txtUsername.Focus();
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string staffName_ = txtNamaStaff.Text;
            string staffPhone_ = txtPhone.Text;
            string staffEmail_ = txtEmail.Text;
            string staffUsername_ = txtUsername.Text.ToLower();
            string staffPassword_ = txtPassword.Text.ToLower();
            string staffId_ = txtStaffID.Text;
            string staffUserId_ = txtUserID.Text;
            if(staffName_ != "")
            {
                Lib.StaffUser staff = new Lib.StaffUser();
                if (flagAdd)
                {
                    if (staffUsername_ != "" && staffPassword_ != "")
                    {
                        if (ValidateUsername())
                        {
                            staff.StaffName = staffName_;
                            staff.StaffPhone = staffPhone_;
                            staff.StaffEmail = staffEmail_;
                            staff.Username = staffUsername_;
                            staff.Password = staffPassword_;
                            if (Lib.StaffControl.CRUD(0, staff))
                            {

                                Lib.NotificationMessage.Notification = "Berhasil menambahkan user";
                            }
                            else
                            {
                                Lib.NotificationMessage.Notification = "Berhasil menambahkan user";
                            }
                            staffLoad();
                            clear();
                            disabled();
                        }
                        else
                        {
                            MessageBox.Show("Username sudah ada, harap ganti dengan username lain", "Konfirmasi");
                        }
                    }else
                    {
                        MessageBox.Show("Username dan Password tidak boleh kosong", "Konfirmasi");
                    }
                    
                }
                if (flagEdit)
                {
                    if (staffId_ != "" && staffUserId_ != "")
                    {
                        if (ValidateUsername())
                        {
                            staff.StaffName = staffName_;
                            staff.StaffPhone = staffPhone_;
                            staff.StaffEmail = staffEmail_;
                            staff.Username = staffUsername_;
                            staff.Password = staffPassword_;
                            staff.UserID = Convert.ToInt32(staffUserId_);
                            staff.StaffID = Convert.ToInt32(staffId_);
                            if (Lib.StaffControl.CRUD(1, staff, flagChangePassword))
                            {
                                Lib.NotificationMessage.Notification = "Berhasil mengedit user";
                            }
                            else
                            {
                                Lib.NotificationMessage.Notification = "Berhasil mengedit user";
                            }
                            staffLoad();
                            clear();
                            disabled();
                        }
                        else
                        {
                            MessageBox.Show("Username sudah ada, harap ganti dengan username lain", "Konfirmasi");
                            return;
                        }
                    }else
                    {
                        MessageBox.Show("Harap pilih staff terlebih dahulu", "Konfirmasi");
                        dgvStaff.Focus();
                        return;
                    }
                }
                if (flagDelete)
                {
                    if (staffId_ != "" && staffUserId_ != "")
                    {
                        staff.StaffName = staffName_;
                        staff.StaffPhone = staffPhone_;
                        staff.StaffEmail = staffEmail_;
                        staff.Username = staffUsername_;
                        staff.Password = staffPassword_;
                        staff.UserID = Convert.ToInt32(staffUserId_);
                        staff.StaffID = Convert.ToInt32(staffId_);
                        if (Lib.StaffControl.CRUD(2, staff))
                        {
                            Lib.NotificationMessage.Notification = "Berhasil menghapus user";
                        }
                        else
                        {
                            Lib.NotificationMessage.Notification = "Berhasil menghapus user";
                        }
                        staffLoad();
                        clear();
                        disabled();
                    }
                    else
                    {
                        MessageBox.Show("Harap pilih staff terlebih dahulu", "Konfirmasi");
                        dgvStaff.Focus();
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Nama Staff tidak boleh kosong", "Konfirmasi");
                txtNamaStaff.Focus();
                return;
            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            clear();
            enabled();
            txtPassword.Text = "";
            txtPassword.Enabled = true;
            flagAdd = true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            clear();
            staffLoad();
        }

        private void btnCancelPassword_Click(object sender, EventArgs e)
        {
            flagChangePassword = false;
            btnViewPassword.Visible = false;
            btnCloseViewPassword.Visible = false;
            txtPassword.UseSystemPasswordChar = true;
            txtPassword.Text = "";
            txtPassword.Enabled = false;
            btnUnlockPassword.Visible = true;
        }

        private void btnUnlockPassword_Click(object sender, EventArgs e)
        {
            flagChangePassword = true;
            txtPassword.Enabled = true;
            btnViewPassword.Visible = true;
            btnUnlockPassword.Visible = false;
        }

        private void btnViewPassword_Click(object sender, EventArgs e)
        {
            btnCloseViewPassword.Visible = true;
            txtPassword.UseSystemPasswordChar = false;
            btnViewPassword.Visible = false;
        }

        private void btnCloseViewPassword_Click(object sender, EventArgs e)
        {
            btnViewPassword.Visible = true;
            txtPassword.UseSystemPasswordChar = true;
            btnCloseViewPassword.Visible = false;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            enabled();
            flagEdit = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtStaffID.Text != "")
            {
                flagDelete = true;
                enabled();
            }
            else
            {
                MessageBox.Show("Harap pilih staff terlebih dahulu!", "Konfirmasi");
                dgvStaff.Focus();
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void dgvStaff_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtStaffID.Text = dgvStaff[0, e.RowIndex].Value.ToString();
                txtNamaStaff.Text = dgvStaff[1, e.RowIndex].Value.ToString();
                txtPhone.Text = dgvStaff[2, e.RowIndex].Value.ToString();
                txtEmail.Text = dgvStaff[3, e.RowIndex].Value.ToString();
                txtUserID.Text = dgvStaff[4, e.RowIndex].Value.ToString();
                txtUsername.Text = dgvStaff[5, e.RowIndex].Value.ToString();
                txtPassword.Text = "New Password";
                btnUnlockPassword.Visible = true;
                btnCancelPassword.Visible = true;
            }
        }

        private void clear()
        {
            flagAdd = false; flagEdit = false; flagDelete = false;
            txtNamaStaff.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtPhone.Text = "";
            txtSearch.Text = "";
            txtUsername.Text = "";
            txtStaffID.Text = "";
            txtUserID.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Cancel ?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                clear();
                disabled();
                btnCloseViewPassword.PerformClick();
                
            }
            else
            {
                return;
            }
        }
    }
}
