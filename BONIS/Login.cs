﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Threading;
using Tulpep.NotificationWindow;

namespace BONIS
{
    public partial class Login : Form
    {

        #region shadowborder
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
         );

        [DllImport("dwmapi.dll")]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS pMarInset);

        [DllImport("dwmapi.dll")]
        public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref int attrValue, int attrSize);

        [DllImport("dwmapi.dll")]
        public static extern int DwmIsCompositionEnabled(ref int pfEnabled);

        private bool m_aeroEnabled;
        private const int CS_DROPSHADOW = 0x60C2F0;
        private const int WM_NCPAINT = 0x0085;
        private const int WM_ACTIVATEAPP = 0x001C;

        public struct MARGINS
        {
            public int leftWidth;
            public int rightWidth;
            public int topHeight;
            public int bottomHeight;
        }

        private const int WM_NCHITTEST = 0x84;
        private const int HTCLIENT = 0x1;
        private const int HTCAPTION = 0x2;

        protected override CreateParams CreateParams
        {
            get
            {
                m_aeroEnabled = CheckAeroEnabled();

                CreateParams cp = base.CreateParams;
                if (!m_aeroEnabled)
                    cp.ClassStyle |= CS_DROPSHADOW;

                return cp;
            }
        }

        private bool CheckAeroEnabled()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                int enabled = 0;
                DwmIsCompositionEnabled(ref enabled);
                return (enabled == 1) ? true : false;
            }
            return false;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCPAINT:
                    if (m_aeroEnabled)
                    {
                        var v = 2;
                        DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        MARGINS margins = new MARGINS()
                        {
                            bottomHeight = 1,
                            leftWidth = 1,
                            rightWidth = 1,
                            topHeight = 1
                        };
                        DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
                case WM_NCHITTEST:
                    m.Result = (IntPtr)(HT_CAPTION);
                    break;
                default:
                    if (m_aeroEnabled)
                    {
                        var v = 2;
                        DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        MARGINS margins = new MARGINS()
                        {
                            bottomHeight = 1,
                            leftWidth = 1,
                            rightWidth = 1,
                            topHeight = 1
                        };
                        DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
            }
            base.WndProc(ref m);

            if (m.Msg == WM_NCHITTEST && (int)m.Result == HTCLIENT)
                m.Result = (IntPtr)HTCAPTION;

        }
        #endregion


        private RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();
        private string SqliteCon = Properties.Settings.Default.InventorySqliteConnectionString;
        public Login()
        {
            InitializeComponent();
            SetPlaceHolder(txtUser, "username", panel11);
            SetPlaceHolder(txtPass, "password", panel12);
        }

        private void SetPlaceHolder(Control control, string PlaceHolderText, Control panel)
        {
            control.Text = PlaceHolderText;
            control.GotFocus += delegate (object sender, EventArgs args)
            {
                if (control.Text == PlaceHolderText)
                {
                    control.Text = "";
                }
                panel.BackColor = Color.SkyBlue;
                control.ForeColor = Color.Black;
            };
            control.LostFocus += delegate (object sender, EventArgs args)
            {
                if (control.Text.Length == 0)
                {
                    control.Text = PlaceHolderText;
                }
                panel.BackColor = Color.FromArgb(60, 18, 114, 214);
                control.ForeColor = Color.Gray;
            };
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == "" || txtPass.Text == "")
            {
                MessageBox.Show("Username dan password tidak boleh kosong");
            }
            else
            {
                checkLogin();
            }
        }

        private async void checkLogin()
        {
            var popup = new PopupNotifier();
            popup.TitleText = "Notifikasi";
            popup.IsRightToLeft = false;
            Loading load_ = new Loading();
            this.BeginInvoke(new Action(() => load_.ShowDialog(this)));
            bool res = false;
            await Task.Run(new Action(() =>
            {
                res = Lib.ManageUser.CheckLogin(txtUser.Text, txtPass.Text);
            }));
            if (res)
            {
                popup.ContentText = "Berhasil login";
                popup.Popup();
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                popup.ContentText = Lib.ManageUser.ErrMessage;
                popup.IsRightToLeft = false;
            }
            popup.Popup();
            load_.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Anda yakin ingin menutup aplikasi ini ?", "Keluar Aplikasi", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }

        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }
        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }



        #region moveablepanel
        /* Moveable panel */
        int panelMoveM, MValX, MValY;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            panelMoveM = 1;
            MValX = e.X;
            MValY = e.Y;
        }

        private void panel13_MouseMove(object sender, MouseEventArgs e)
        {
            if (panelMoveM == 1)
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
            }
        }

        private void panel13_MouseUp(object sender, MouseEventArgs e)
        {
            panelMoveM = 0;
        }

        private void panel13_MouseDown(object sender, MouseEventArgs e)
        {
            panelMoveM = 1;
            MValX = e.X;
            MValY = e.Y;
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            panelMoveM = 0;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (panelMoveM == 1)
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            panelMoveM = 1;
            MValX = e.X;
            MValY = e.Y;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panelMoveM = 0;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (panelMoveM == 1)
            {
                this.SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
            }
        }
        #endregion
    }
}
